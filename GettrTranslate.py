import GettrSpider
import scrapy
import DTime
import json
import Log

class GettrTranslate(GettrSpider.GettrSpider):
    def start(self):
        self.log.setTextColor(Log.COLOR256_211)

    def handle(self, X):
        if X[0] == 'response':
            self.handleTranslate(X[1])
            return

        self.log.W("unknown", X)

    def handleClose(self):
        pass

    def handleTranslate(self, X):
        meta = X.meta
        pid = meta["pid"]
        fieldName = meta["fieldName"]

        try:
            j = json.loads(X.text)
            item = {}
            item["pid"] = j['pstid']
            if pid != item["pid"]:
                self.log.E("pid not match", pid, item["pid"], j)
            item[fieldName] = j['txt']
            postDB = self.getDB('post')
            if postDB.set(item):
                self.log.D("update translate", pid, minGap=7)
        except:
            self.log.E(X)
        return


        queryUser = accountDB.get(queryUid)

        j = json.loads(X.text)
        # query repost cdate
        repostCdate = {}
        try:
            d = j["result"]["data"]["list"]
            for lst in d:
                if "activity" in lst:
                    pid = lst["activity"]["pstid"]
                    self.extractDatetime(repostCdate, pid, lst, "cdate")
        except:
            pass

        try:
            d = j["result"]["aux"]["post"]
        except:
            return

        isSkipped = False
        isUpdated = False
        for pid in d:
            post = d[pid]
            item = {}
            item["pid"] = pid
            self.extractDatetime(item, "cdate", post, "cdate")
            if pid in repostCdate:
                item["udate"] = repostCdate[pid]
            else:
                item["udate"] = item["cdate"]
            self.extractStr(item, "uid", post, "uid")

            user = accountDB.get(item["uid"])
            if not user:
                self.log.D("[New user]", item["uid"], minGap = 7, timeIt = True)
                self.sendAccountQuery(item["uid"])

            if item['udate'] <= minTime or maxTime < item['udate']:
                isSkipped = True
                #self.log.W("skip post", queryUid, pid, minTime, item, minGap=3)
                continue

            self.extractStr(item, "txt", post, "txt")
            self.extractStr(item, "txt_lang", post, "txt_lang")
            self.extractStr(item, "prevsrc", post, "prevsrc")
            self.extractStr(item, "previmg", post, "previmg")
            self.extractStr(item, "ttl", post, "ttl")
            self.extractStr(item, "dsc", post, "dsc")
            self.extractStr(item, "vid", post, "vid")
            self.extractInt(item, "vid_dur", post, "vid_dur")
            self.extractInt(item, "vid_wid", post, "vid_wid")
            self.extractInt(item, "vid_hgt", post, "vid_hgt")
            if "prevsrc" in item:
                txt = ""
                if "ttl" in item:
                    txt += item["ttl"]
                if "dsc" in item:
                    txt += item["dsc"]
                txt = txt.replace('\n','')
                item["prev_lang"] = self.lid.getLang(txt)

                if 'vid' not in item:
                    if item['prevsrc'].find('gtv.org/video/')>0:
                        item['vid']="[GTV]"

            self.extractInt(item, "lkbpst", post, "lkbpst")
            self.extractInt(item, "cm", post, "cm")
            self.extractInt(item, "shbpst", post, "shbpst")

            if "htgs" in post:
                item["htgs"] = '#'.join(post["htgs"])
            if "utgs" in post:
                item["utgs"] = '@'.join(post["utgs"])

            item['max_views'] = queryUser['gettr_follower']

            # append the max_views from database
            oldItem = postDB.get(pid)
            if oldItem:
                item['max_views'] += oldItem['max_views']



            if postDB.set(item):
                self.log.D("update post", pid, timeIt=True, minGap=3)
                isUpdated = True
            continue

        if isUpdated and not isSkipped:
            offset +=20
            self.log.D("next page", queryUid, offset, minTime, maxTime)
            self.scanAccountPostPage(queryUid, offset, minTime, maxTime)

            """
            if pid not in self.postSharer:
                self.postSharer[pid] = set()

            self.postSharer[pid] |= set({uid, ouid})

            if self.genReport:
                if ofollower > 0:
                    item["max_views"] = ofollower
                else:
                    item["max_views"] = 0

            if user and "gettr_follower" in user:
                item["gettr_follower"] = user["gettr_follower"]
            self.queueItem(item)
            isUpdated = True

            #if "txt_lang" in item and item["txt_lang"] != "en":
            if "translates" in post:
                if "en" in post["translates"]:
                    if "translatedPath" in post["translates"]["en"]:
                        yield scrapy.Request(post["translates"]["en"]["translatedPath"], callback = self.parseTranslate_en)
                if "zh-Hans" in post["translates"]:
                    if "translatedPath" in post["translates"]["zh-Hans"]:
                        yield scrapy.Request(post["translates"]["zh-Hans"]["translatedPath"], callback = self.parseTranslate_zhs)
                if "zh-Hant" in post["translates"]:
                    if "translatedPath" in post["translates"]["zh-Hant"]:
                        yield scrapy.Request(post["translates"]["zh-Hant"]["translatedPath"], callback = self.parseTranslate_zht)

        if not isUpdated:
            return

        try:
            d = j["result"]["data"]["list"]
        except:
            return
        for lst in d:
            item = {}
            self.extractDatetime(item, "cdate", lst, "cdate")
            #self.D("check post time cdate", item["cdate"], "minDate", self.crawlerMinDate)
            if item["cdate"] < self.crawlerMinDate:
                return

        m = re.search("^(.*offset=)(.*)(&max=.*$)", response.url)
        if m:
            offset = int(m.group(2))
            newUrl = m.group(1)+str(offset + postInfo.queryMaxPerPage)+ m.group(3)
            self.D("next page", newUrl, minGap = 10, timeIt = True)
            yield scrapy.Request(newUrl)
            """



if __name__ == '__main__':
    trend = SpiderPost('post', "[Post]")
    trend.stop()
    trend = None

