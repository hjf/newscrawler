import Scrat
import SpiderQueen
import GettrAccount
import GettrFollow
import GettrPost
import GettrTranslate
import GettrTrend


def main():
    spiderQueen = SpiderQueen.SpiderQueen("queen")
    spiderQueen.register()
    spiderQueen.register( 20, GettrTrend.GettrTrend("trend", commander=True))
    spiderQueen.register(  1, GettrPost.GettrPost("post", commander=True))
    spiderQueen.register(  2, GettrTranslate.GettrTranslate("trans"))
    spiderQueen.register( 50, GettrFollow.GettrFollow("follo", commander=True))
    spiderQueen.register( 50, GettrAccount.GettrAccount("accou"))
    spiderQueen.waitRequestPrepare()
    Scrat.run_forever(spiderQueen)
    spiderQueen.log = None
    spiderQueen = None

if __name__ == '__main__':
    main()
