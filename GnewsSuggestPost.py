import GnewsSpider
import scrapy
import json
import traceback

class GnewsSuggestPost(GnewsSpider.GnewsSpider):
    def start(self):
        url = "https://api.gnews.org/u/feed/suggest"
        self.sendQueenRequest(url, 'suggp')

    def handle(self, X):
        if X[0] == 'response':
            j = json.loads(X[1].text)
            try:
                d = j["result"]["aux"]["uinf"]
                self.sendAccountData(X[1])
            except:
                pass

            try:
                postDB = self.getDB('post')
                d = j["result"]["aux"]["post"]

                for pid in d:
                    if not postDB.get(pid):
                        self.sendPostQuery(pid)
            except:
                traceback.print_exc()
                pass

            self.sendQueenUnregister()

    def handleClose(self):
        pass

