import GnewsSpider
import scrapy
import json
import traceback

class GnewsTrend(GnewsSpider.GnewsSpider):
    def start(self):
        url = "https://api.gnews.org/u/feed/trending?offset=0&max=20"
        meta={'offset':0, 'nextoffset': 20, 'offsetgap':1000}
        self.sendQueenRequest(url, 'trend', meta=meta)

    def handle(self, X):
        if X[0] == 'response':
            j = json.loads(X[1].text)
            try:
                d = j["result"]["aux"]["uinf"]
                self.sendAccountData(X[1])
            except:
                pass

            isPaged = False
            try:
                offset = X[1].meta['offset']
                nextoffset = X[1].meta['nextoffset']
                offsetgap = X[1].meta['offsetgap']
                self.log.D("Trending offset", offset, timeIt=True, minGap=7)

                postDB = self.getDB('post')
                d = j["result"]["aux"]["post"]
                for pid in d:
                    if not postDB.get(pid):
                        self.sendPostQuery(pid)
                        if not isPaged: # search next page
                            if offset + 20 >= nextoffset: # trigger next whole gap pages
                                newOffset = offset
                                newNextOffset = nextoffset + offsetgap
                                while True:
                                    newOffset += 20
                                    if newOffset >= newNextOffset:
                                        break
                                    meta={'offset':newOffset, 'nextoffset': newNextOffset, 'offsetgap':offsetgap}
                                    url = f"https://api.gnews.org/u/feed/trending?offset={newOffset}&max=20"
                                    self.sendQueenRequest(url, 'trend', meta=meta)
                            isPaged = True
            except:
                traceback.print_exc()
                pass

            #if offset + 20 >= nextoffset and not isPaged:
            #    self.sendQueenUnregister()

    def handleClose(self):
        pass

