import DTime
import Log
import pymongo

class DbConnection():
    def __init__(self, connection  = "mongodb://127.0.0.1:27017", connect = False, memVirtualLimit = 77):
        self.Log = Log.Log()
        self.connection = pymongo.MongoClient(connection, connect = connect)
        self.session = self.connection.start_session()
        self.refreshTime = DTime.now()
        self.memVirtualLimit = memVirtualLimit
        self.Log.D("connect", self.connection, self.session)

    def __del__(self):
        self.Log.D("disconnect", self.connection, self.session)
        self.session = None
        self.connection = None

    def __getitem__(self, key):
        return self.connection[key]

def test_connect():
    L = Log.Log()
    conn = DbConnection()
    L.D("mongo version", pymongo.version)

if __name__ == "__main__":
    test_connect()
