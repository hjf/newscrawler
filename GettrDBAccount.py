import DictDB

class GettrDBAccount(DictDB.DictDB):

    def __init__(self,
                 dbName         = "gettr",
                 tableName      = "account",
                 keyField       = "uid",
                 drop           = False,
                 minCache       = 7,
                 maxCache       = 0,
                 ioCache        = 777,
                 maxMemPercent  = 77.7):
        super().__init__(dbName = dbName, tableName=tableName, keyField=keyField, drop=drop, minCache=minCache,
                        maxCache=maxCache, ioCache=ioCache, maxMemPercent=maxMemPercent)

    def createTable(self):
        self.rawDB.execSQL("""CREATE TABLE IF NOT EXISTS {} (
            uid text primary key,
            status text,
            location text,
            lang text,
            infl int DEFAULT 0,
            lkspst int DEFAULT 0,
            lkscm int DEFAULT 0,
            shspst int DEFAULT 0,
            twitter_follower int DEFAULT 0,
            twitter_following int DEFAULT 0,
            gettr_follower int DEFAULT 0,
            gettr_following int DEFAULT 0,
            cdate timestamp with time zone,
            crawler_time timestamp with time zone,
            crawler_post_time timestamp with time zone,
            crawler_follow_time timestamp with time zone
            );
            CREATE INDEX account_crawler_time ON account (crawler_time);
            CREATE INDEX account_crawler_post_time ON account (crawler_post_time);
            CREATE INDEX account_crawler_follow_time ON account (crawler_follow_time);
            """.format(self.tableName))

if __name__ == '__main__':
    db = GettrDBAccount(drop=True)
    db = None
