import Spider
import scrapy
import json
import time
from scrapy.http.cookies import CookieJar
#import tweepy

class SpiderTwPost(Spider.Spider):
    def start(self):
        #meta = {'LoginStage': 0}
        meta = {}
        cookiejar = CookieJar()
        meta={'cookiejar': cookiejar}
        url = "https://twitter.com/torontoStar" #moszcat@gmail.com
        #authorization
        #Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA
        bearer = "Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA"

        #auth = tweepy.OAuth2BearerHandler(bearer)
        #client = tweepy.Client("Bearer Token here")
        #self.log.D(client)
        #api = tweepy.API(auth)
        #self.log.D(api)
        #url = "https://www.twitter.com/"
        #self.sendQueenRequest(url, 'twpst', meta=meta)
        url = "https://twitter.com/torontoStar"
        self.sendQueenRequest(url, 'twpst', meta=meta)

    def handle(self, X):
        if X[0] == 'webdriver':
            wd = X[1]
            self.log.D(wd.page_source)
            return

        if X[0] == 'response':
            response = X[1]
            meta = X[2]
            cookiejar = meta['cookiejar']
            if 'stage' not in meta:
                cookiejar.extract_cookies(response, response.request)
                for cookie in cookiejar:
                    self.log.D("cookie", cookie)
                meta['stage'] = 1
                self.log.D("stage 0 headers", X[1].headers)
                bearer = "Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA"
                headers = {}
                headers['Authorization'] = bearer
                url = "https://twitter.com/i/api/graphql/yizglwBmu73cHxAcY9CDpA/UserTweets?variables=%7B%22userId%22%3A%2212848262%22%2C%22count%22%3A40%2C%22includePromotedContent%22%3Atrue%2C%22withQuickPromoteEligibilityTweetFields%22%3Atrue%2C%22withSuperFollowsUserFields%22%3Atrue%2C%22withDownvotePerspective%22%3Afalse%2C%22withReactionsMetadata%22%3Afalse%2C%22withReactionsPerspective%22%3Afalse%2C%22withSuperFollowsTweetFields%22%3Atrue%2C%22withVoice%22%3Atrue%2C%22withV2Timeline%22%3Atrue%2C%22__fs_dont_mention_me_view_api_enabled%22%3Afalse%2C%22__fs_interactive_text_enabled%22%3Atrue%2C%22__fs_responsive_web_uc_gql_enabled%22%3Afalse%7D"
                self.sendQueenRequest(url, 'twpst', meta=meta, headers=headers)
            else:
                self.log.D("stage1 headers", X[1].headers)
                self.log.D("text", X[1].text)

    def handleClose(self):
        pass


