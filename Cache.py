import collections
import Log
import threading
import time
import psutil
import concurrent.futures

class Cache():
    def __init__(self, name='', minCache=1, maxCache=0, maxMemPercent=77.7, ioCache=7777, getFunc=None, setFunc=None, closeFunc=None, queryFunc=None):
        self.log = Log.Log(who='cache')
        #self.log = Log.Log(Log.VERBOSE_LEVEL)
        self.log.V("Cache (minCache", minCache,
                        ", maxCache", maxCache,
                        ", maxMemPercent", maxMemPercent,
                        ", getFunc", getFunc,
                        ", setFunc", setFunc,
                        ", queryFunc", queryFunc,
                        ")")
        self.log.setTextColor(Log.COLOR256_86)
        self.name = name
        self.minCache = minCache
        self.maxCache = maxCache
        self.ioCache = ioCache
        self.maxMemPercent = maxMemPercent
        self.getFunc = getFunc
        self.setFunc = setFunc
        self.queryFunc = queryFunc
        self.closeFunc = closeFunc
        self.cacheDictLock = threading.Lock()
        self.cacheDict = collections.OrderedDict()
        self.waitFlushSet = set()

        self.flushDoneEvent = threading.Event() # set when flush finish
        self.flushLock = threading.Lock()  # lock when flushing
        self.flushCnt = 0

    def size(self):
        with self.cacheDictLock:
            return len(self.cacheDict)

    def dirtySize(self):
        with self.cacheDictLock:
            return len(self.waitFlushSet)

    def stop(self):
        self.log.D("stop()", self.name)
        self.flush(True)
        if self.closeFunc:
            self.closeFunc()

        self.getFunc = None
        self.setFunc = None
        self.queryFunc = None
        self.closeFunc = None

    def __del__(self):
        self.log.D("Cache __del__", self.name)

    def flushCallback(self, kv):
        with self.cacheDictLock:
            if kv in self.waitFlushSet:
                self.waitFlushSet.remove(kv)
            else:
                self.log.W("flushCallback can't remove", kv)
            self.flushCnt -= 1
            self.log.D("Cache "+ self.name+" callback", self.flushCnt, timeIt=True, minGap=7)
            if self.flushCnt == 0:
                self.flushDoneEvent.set()
                self.flushLock.release()
                self.log.D("Cache "+ self.name+" flush","done", timeIt=True)

    def flush(self, block=False):
        if not self.setFunc:
            return False

        if not block:
            with self.cacheDictLock:
                if self.flushCnt > 0: #during flushing
                    return False

        self.flushLock.acquire() # Hold lock to enter flush stage
        self.flushDoneEvent.clear()

        with self.cacheDictLock:
            assert self.flushCnt == 0
            if len(self.waitFlushSet) > 0:
                for kv in self.waitFlushSet:
                    item = self.cacheDict[kv]
                    if self.setFunc:
                        self.log.V("setFunc", kv, item)
                        self.flushCnt += 1
                        self.setFunc(kv, item, self.flushCallback)
                self.log.D("Cache "+ self.name+" flush", "begin", self.flushCnt, "entries. blocking", block, timeIt=True, sum=self.flushCnt)

            if self.flushCnt == 0:
                self.flushDoneEvent.set()
                self.flushLock.release()

        if block:
            self.flushDoneEvent.wait()
        return True

    def reduce(self):
        if self.flushCnt > 0: #flushing
            return False

        if self.dirtySize() > self.ioCache:
            if not self.flush():
                return

        cacheSize = len(self.cacheDict)
        self.log.V("reduce() min:", self.minCache, "curr:", cacheSize, "max:", self.maxCache)
        if cacheSize <= self.minCache:
            return

        if self.maxCache == 0: # detect memory presure
            if psutil.virtual_memory().percent > self.maxMemPercent:
                self.maxCache = cacheSize
                self.log.V("reduce() reach memory presure, set maxCache =", self.maxCache)
            return
        
        if cacheSize <= self.maxCache:
            return

        # flush all before reduce
        if self.dirtySize() > 0:
            self.log.D("flush before reduce()", self.dirtySize(), minGap=1)
            self.flush()
            return

        with self.cacheDictLock:
            dropCount = cacheSize - self.maxCache
            self.log.V("reduce() cache drop count", dropCount)
            for i in range(dropCount):
                (kv, item) = self.cacheDict.popitem(last=False)
                if kv in self.waitFlushSet:
                    self.waitFlushSet.remove(kv)
                    if self.setFunc:
                        self.log.W("setFunc during reduce()", kv, item, minGap=1)
                        self.setFunc(kv, item)

    def get(self, kv, needClone=True):
        with self.cacheDictLock:
            if kv in self.cacheDict:
                self.cacheDict.move_to_end(kv)
                if needClone and self.cacheDict[kv]:
                    return self.cacheDict[kv].copy()
                else:
                    return self.cacheDict[kv]

        if self.getFunc:
            item = self.getFunc(kv)
            self.log.V("getFunc", kv, item)
            if item:
                with self.cacheDictLock:
                    self.cacheDict[kv] = item
                    self.cacheDict.move_to_end(kv)
                if needClone:
                    return self.cacheDict[kv].copy()
                else:
                    return self.cacheDict[kv]
            else:
                self.cacheDict[kv] = None
        return None

    def set(self, kv, item, allowReplace = True, isUnion = True):
        if (isUnion) or (not allowReplace):
            if kv not in self.cacheDict:
                self.get(kv, needClone=False) # try to load as initialize value

            if (not allowReplace) and (kv in self.cacheDict) and (self.cacheDict[kv] != None):
                self.log.V("not allowReplace", kv, item)
                return False

        with self.cacheDictLock:
            if isUnion and (kv in self.cacheDict) and (self.cacheDict[kv] != None):
                isChanged = False
                for i in item:
                    if i not in self.cacheDict[kv]: # new field
                        isChanged = True
                        break
                    if item[i] != self.cacheDict[kv][i]: # new value with same field
                        self.log.V("Diff", kv,i, item[i], self.cacheDict[kv][i])
                        isChanged = True
                        break
                isUpdated = isChanged and allowReplace
                if isUpdated:
                    self.cacheDict[kv] = self.cacheDict[kv].copy()
                    self.cacheDict[kv].update(item)
                    self.cacheDict.move_to_end(kv)
                    self.waitFlushSet.add(kv)
                    self.log.V("update", kv, self.cacheDict[kv])
                else:
                    self.log.V("keep no update", kv, item)
            else:
                self.cacheDict[kv] = item.copy()
                self.cacheDict.move_to_end(kv)
                self.waitFlushSet.add(kv)
                self.log.V("add", kv, self.cacheDict[kv])
                isUpdated = True

        if isUpdated:
            self.reduce()

        return isUpdated

    def query(self, sql, *args):
        if self.queryFunc:
            X = self.queryFunc(sql, *args)
            if X:
                try:
                    keys = []
                    with self.cacheDictLock:
                        for (kv, item) in X:
                            if kv in self.cacheDict:
                                item.update(self.cacheDict[kv])
                            self.cacheDict[kv] = item
                            self.cacheDict.move_to_end(kv)
                            keys.append(kv)
                        return keys
                except:
                    self.log.W(X)
                    traceback.print_exc()
                    os._exit(1)
        return None


def test_get():
    def get(kv):
        return {'v':kv}

    cache = Cache(minCache=5, maxCache=10, getFunc=get)
    for i in range(1000):
        X = cache.get(i%77)
        assert (X['v'] == i%77)
        X['v'] = "XX" # modify it should not impact cache

def test_large_size():
    log = Log.Log()
    def get(kv):
        return {'v': "X" * 1024*1024*1024} # generate 1GB
    #Log.setLogLevel(Log.VERBOSE_LEVEL)
    V = psutil.virtual_memory()
    percent = (V.used + 3.0*1024*1024*1024) *100/ V.total  # only allow 4GB
    cache = Cache(minCache=1, maxMemPercent=percent, getFunc=get)
    for i in range(8):
        V = psutil.virtual_memory()
        log.D("cache size", cache.size(), f'{V.percent:f}%', "used:", V.used/1024/1024, "MB")
        X = cache.get(i)
        log.D("cache size", cache.size(), "cost:", (psutil.virtual_memory().used - V.used)/1024/1024, "MB")
        cache.reduce()
        assert (cache.size() <=3 )

def test_dummy_set():
    cache = Cache(maxCache=10)
    data = {}
    for i in range(10000):
        data['v'] = i
        cache.set(i, data)


def test_basic_get_set():
    myDict = {}
    def set(kv, item):
        myDict[kv] = item

    def get(kv):
        if kv in myDict:
            item = myDict[kv]
        else:
            item = None
        return item

    cache = Cache(maxCache=10, getFunc=get, setFunc=set)
    for i in range(30):
        kv = i%10
        X = cache.get(kv)
        if i<10:
            assert (X == None)
            assert (cache.set(kv, {'new':-i}, allowReplace=False)) # None can be replaced
        else:
            assert (X['v'] == i-10)
            assert not cache.set(kv, {'new':-i}, allowReplace=False) # can't be replaced

        cache.set(kv, {'v':i})
        X = cache.get(kv)
        cache.set(kv, {'t':i}, isUnion=False)
        Y = cache.get(kv)
        assert ('v' not in Y)
        assert (Y['t'] == i)

        cache.set(kv, {'v':i, 'q':i+1})
        Z = cache.get(kv)
        assert ('v' in Z)
        assert ('t' in Z)
        assert ('q' in Z)
        assert (Z['q'] == i+1)

def test_get_not_exists():
    getCount = 0
    def get(kv):
        nonlocal getCount
        getCount += 1
        return None
    cache = Cache(getFunc=get)
    cache.get(0)
    cache.get(0)
    cache.get(0)
    cache.get(0)
    cache.get(0)
    assert (getCount == 1)

def test_max_cache():
    L = Log.Log()
    #L.setLogLevel(Log.VERBOSE_LEVEL)
    getCount = 0
    def get(kv):
        nonlocal getCount
        getCount += 1
        L.V("get", kv, getCount)
        return {'v':kv}

    cache = Cache(maxCache=10, getFunc=get)
    for i in range(15):
        cache.get(i)
        cache.reduce()
        assert (cache.size() == min(i+1, 10))
        assert (getCount == i+1)

    for i in reversed(range(15)):
        cache.get(i)
        if i>4:
            assert (cache.size() == 10)
        else:
            assert (cache.size() == 10+(5-i))
    assert (getCount == 15+15-10)
    #Log.setLogLevel(Log.DEBUG_LEVEL)

def test_multithread():
    myMutex = threading.Lock()
    myDict = {}
    def set(kv, data):
        myDict[kv] = data
        #with myMutex:
        #    print("set", kv, data)

    def get(kv):
        if kv in myDict:
            #with myMutex:
            #    print("get", kv, myDict[kv], "success")
            return myDict[kv]
        else:
            #with myMutex:
            #    print("get", kv, "fail")
            return None
    cache = Cache(maxCache=10, setFunc=set, getFunc=get)

    executor = concurrent.futures.ThreadPoolExecutor()

    def X(kv,v):
        cache.set(kv, v)

    def Y(kv,v):
        v1 = cache.get(kv)
        assert v1 == v

    LOOP = 100
    for i in range(LOOP):
        d = {'v':i*2}
        executor.submit(X, i, d)
        executor.submit(Y, i, d)

def test_free():
    class X:
        def __init__(self):
            self.cache = Cache()
    x = X()
    x = None


def test_query():
    def query(sql):
        data = {}
        data[0]={0}
        data[1]={1}
        data[2]={2}
        return data.items()

    cache = Cache(maxCache=10, queryFunc=query)

    for i in cache.query('x'):
        assert cache.get(i) == {i}

    assert cache.get(4) == None

if __name__ == "__main__":
    test_get()
    test_large_size()
    test_dummy_set()
    test_basic_get_set()
    test_get_not_exists()
    test_max_cache()
    test_multithread()
    test_free()
    test_query()
