import GnewsSpider
import scrapy
import json
import traceback

class GnewsHeadline(GnewsSpider.GnewsSpider):
    def start(self):
        url = "https://api.gnews.org/u/feed/headline?offset=0&max=20"
        meta={'offset':0}
        self.sendQueenRequest(url, 'headl', meta=meta)

    def handle(self, X):
        if X[0] == 'response':
            j = json.loads(X[1].text)
            try:
                d = j["result"]["aux"]["uinf"]
                self.sendAccountData(X[1])
            except:
                pass

            isPaged = False
            try:
                postDB = self.getDB('post')
                d = j["result"]["aux"]["post"]

                for pid in d:
                    if not postDB.get(pid):
                        self.sendPostQuery(pid)
                        if not isPaged: # search next page
                            meta =X[1].meta.copy()
                            meta['offset'] +=20
                            url = f"https://api.gnews.org/u/feed/headline?offset={meta['offset']}&max=20"
                            self.log.D("query headline offset", meta['offset'])
                            self.sendQueenRequest(url, 'headl', meta=meta)
                            isPaged = True

            except:
                traceback.print_exc()
                pass

            #if not isPaged:
            #    self.sendQueenUnregister()

    def handleClose(self):
        pass

