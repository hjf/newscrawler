import GettrSpider
import scrapy
import json

class GettrTrend(GettrSpider.GettrSpider):
    def start(self):
        url = "https://api.gettr.com/u/posts/trends?lang=en_us&offset=0&max=100&incl=posts%7Cstats%7Cuserinfo%7Cshared%7Cliked"
        self.sendQueenRequest(url, 'trend')

    def handle(self, X):
        if X[0] == 'response':
            accountDB = self.getDB('account')
            j = json.loads(X[1].text)
            try:
                d = j["result"]["aux"]["post"]
            except:
                return

            for pid in d:
                post = d[pid]
                uid = post["uid"]
                user = accountDB.get(uid)
                if not user:
                    self.sendAccountQuery(uid)
            self.sendQueenUnregister()

    def handleClose(self):
        pass

if __name__ == '__main__':
    trend = GettrTrend("[GettrTrend]")
    trend.stop()
    trend = None

