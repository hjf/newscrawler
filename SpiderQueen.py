from dataclasses import dataclass, field
from typing import Any
import heapq
import Log
import os
import queue
import scrapy
import Spider
import sys
import threading
import traceback
import DTime
import time
import signal

MAX_SCRAPY_THREAD=1024

@dataclass(order=True)
class PQueueItem:
    priority:int
    item: Any=field(compare=False)

class SpiderQueen(Spider.Spider):
    def start(self):
        #self.log.setLogLevel(Log.VERBOSE_LEVEL)
        self.requestQueue = queue.PriorityQueue()
        signal.signal(signal.SIGHUP, self.handleSignal)

    def handleSignal(self, signum, frame):
        self.log.W("Receive SIGHUP, prepare exit")
        self.eventExit.set()

    def waitRequestPrepare(self, timeout=7):
        while timeout > 0:
            s = self.requestQueue.qsize()
            self.log.D("waitRequestPrepare queue:", s, "countdown:", timeout)
            if s > 0:
                break
            time.sleep(1)
            timeout -= 1

    def getMaxScrapyThread(self):
        return max(MAX_SCRAPY_THREAD, self.maxCycleHours)

    def pollRequest(self, block=True, inFlightCnt=0):
        if self.isExit():
            self.log.W("abort pollRequest due to isExit()", minGap=7)
            return None

        maxScrapy = self.getMaxScrapyThread()
        while True:
            try:
                if not block and inFlightCnt > self.maxCycleHours and self.requestQueue.qsize() < maxScrapy:
                    self.log.D("holding request:", inFlightCnt, self.requestQueue.qsize(), timeIt=True, minGap=7)
                    return None

                X = self.requestQueue.get(block, timeout=3)
                if X:
                    meta = X.item[2]
                    target = meta['target']
                    spider = self.getSpider(target)
                    if spider:
                        spider.setLastTimeActive(DTime.now())

                    if self.log.getLogLevel() == Log.VERBOSE_LEVEL:
                        mingap=0
                    else:
                        mingap=7
                    self.log.D("requestQueue.get curPri:", self.maxPriority, "pri:", X.priority, "qsize:",
                               self.requestQueue.qsize(), X.item, timeIt=True, minGap=mingap)
                    return X.item
            except queue.Empty:
                return None
            except:
                traceback.print_exc()
                os._exit(1)

    def handleResponse(self, response):
        if 'target' in response.meta:
            target = response.meta['target']
            spider = self.getSpider(target)
            if spider:
                spider.setLastTimeActive(DTime.now())
        self.sendResponse(response)

    def handle(self, X):
        self.showGlobalQueueSize()
        if X[0] == 'request':
            meta = X[2]
            target = meta['target']
            #reset base
            if self.maxPriority > sys.maxsize/2 and self.requestQueue.qsize() == 0:
                self.log.D("reset priority", self.maxPriority)
                self.maxPriority = 0
                for x in self.priority:
                    self.priority[x] = 0
            self.priority[target] += self.cycleHours[target]
            priority = self.priority[target]
            self.maxPriority = max(self.maxPriority, priority)
            spider = self.getSpider(target)
            if spider:
                spider.setLastTimeActive(DTime.now())

            self.requestQueue.put(PQueueItem(priority, X))
            self.log.D("requestQueue.put curPri:",self.maxPriority, "pri:", priority, "qsize:", self.requestQueue.qsize(),
                       "target:", target, timeIt=True, minGap=7)
            return

        if X[0] == 'response':
            response = X[1]
            target = response.meta['target']
            spider = self.getSpider(target)
            if spider:
                spider.setLastTimeActive(DTime.now())
                self.log.D("transfer to", target, X)
                spider.enque(response)
            else:
                self.log.W("can't transfer to", target, X)
            return

        if X[0] == 'unregister':
            target = X[1]
            self.unregister(target)
            return

        self.log.W("unknown response", X)

    def handleClose(self):
        self.stopAllSpiders()
        #self.requestQueue = None
        #self.log = None

def main():
    spider = SpiderQueen("queen")

    for x in range(10):
        spider.enque(('handle',x))
    spider.stop()

if __name__ == '__main__':
    main()


