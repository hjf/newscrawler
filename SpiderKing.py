import Spider
import Log
from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager
#from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.firefox.service import Service as FirefoxService
#from selenium.webdriver.chrome.service import Service as ChromeService

class SpiderKing(Spider.Spider):
    def start(self):
        self.log.setTextColor(Log.COLOR256_111)
        #self.service = FirefoxService(executable_path=GeckoDriverManager().install())
        self.options = webdriver.FirefoxOptions()
        self.options.headless = True

        #self.options = webdriver.ChromeOptions()
        #self.options.headless = True
        #self.options.add_argument("--remote-debugging-port=9222")
        #self.driver = webdriver.Firefox(service=service, options=options)
        #self.log.D("firefox driver start...", self.driver)
        #self.driver.get("https://twitter.com/i/flow/login")
        #self.log.D("title:", self.driver.title)
        #self.log.D("url:", self.driver.current_url)
        #self.log.D("src:", self.driver.page_source)
        #self.driver = None

    def handle(self, X):
        if X[0] == 'request':
            url = X[1]
            meta = X[2]
            if "webdriver" in meta:
                driver = meta["webdriver"]
            else:
                service = FirefoxService(executable_path=GeckoDriverManager().install())
                #service = ChromeService(executable_path=ChromeDriverManager().install())
                driver = webdriver.Firefox(service=service, options=self.options)
                #driver = webdriver.Chrome(service=service, options=self.options)
                meta["webdriver"] = driver
            driver.get(url)
            self.enque(meta["target"],("webdriver", driver, meta))

    def handleClose(self):
        pass
        #self.driver.quit()
        #self.log.D("firefox driver end...")

def main():
    king = SpiderKing("king")
    king.start()

if __name__ == '__main__':
    main()
