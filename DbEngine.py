import DTime
import gc
import Log
import os
import psycopg
import psycopg.rows
import psycopg_pool
import queue
import sys
import threading
import time
import traceback
from dataclasses import dataclass, field
from typing import Any
from concurrent.futures import ThreadPoolExecutor

@dataclass(order=True)
class PQueueItem:
    priority:int
    item: Any=field(compare=False)

class DbEngineWorker:
    def __init__(self, dbName = 'test'):
        self.log = Log.Log(who='dbwrk')
        #self.log = Log.Log(Log.VERBOSE_LEVEL)
        self.log.V("__init__")
        self.log.setTextColor(Log.COLOR256_31)
        self.dbName = dbName
        self.dbInit()

        self.pqueue = queue.PriorityQueue()
        self.queueDead = threading.Event()
        self.executor = ThreadPoolExecutor(thread_name_prefix='db')
        threading.Thread(target = self.queueLoop, name='dbwrk').start()

    def dbInit(self):
        self.connPool = psycopg_pool.ConnectionPool(conninfo=f"dbname={self.dbName} user=postgres")

        self.log.V(self.connPool)
        with self.connPool.connection() as conn:
            self.log.D(f"PostgreSQL version:{conn.info.server_version}, dbName:{self.dbName}")

    def getDbName(self):
        return self.dbName

    def stop(self):
        self.log.V("stop()")
        if not self.queueDead.is_set():
            self.log.V("stop queueThread+")
            self.pqueue.put(PQueueItem(9, None))
            self.queueDead.wait()
            self.log.V("stop queueThread-")
        self.log.V("stopped")

    def __del__(self):
        self.log.D("DbEngineWorker __del__")

    def enque(self, X):
        if X:
            priority = 0
            if X[0] == 'get':
                priority = 0
            elif X[0] == 'count':
                priority = 0
            elif X[0] == 'drop':
                priority = 0
            elif X[0] == 'exec':
                priority = 0
            elif X[0] == 'query':
                priority = 0
            elif X[0] == 'exist':
                priority = 0
            elif X[0] == 'set':
                priority = 1
            self.pqueue.put(PQueueItem(priority, X))
            self.log.D("enque", self.pqueue.qsize(), X, timeIt=True, minGap=17)

    def queueLoop(self):
        self.log.D("DbEngineWorker begin")

        while True:
            try:
                P = self.pqueue.get(timeout=7)
                X = P.item
                if X:
                    self.executor.submit(self.queueHandle, X)
                    #self.queueHandle(X)
                else:
                    self.log.D("Receive EXIT")
                    break
            except queue.Empty:
                pass
            #except:
            #    traceback.print_exc()
            #    os._exit(1)

        self.log.D("DbEngineWorker exit")
        self.queueDead.set()

    def queueHandle(self, X):
        try:
            #self.log.D(f"DBWorker-{X[0]}", timeIt=True, bypass=True)
            if X[0] == 'get':
                self.get(X)
            elif X[0] == 'count':
                self.count(X)
            elif X[0] == 'drop':
                self.dropTable(X)
            elif X[0] == 'exec':
                self.execSQL(X)
            elif X[0] == 'query':
                self.query(X)
            elif X[0] == 'exist':
                self.existTable(X)
            elif X[0] == 'set':
                self.set(X)
            else:
                self.log.W("Unknown", X)
            #self.log.D(f"DBWorker-{X[0]}", timeIt=True, minGap=7)
        except:
            traceback.print_exc()
            os._exit(1)

    def isTableExist(self, conn, tableName):
        cur = conn.execute("select * from information_schema.tables where table_name=%s;", (tableName,))
        row = cur.fetchone()
        self.log.V("isTableExist", row)
        return row != None

    def existTable(self, X):
        (_, getQueue, tableName) = X
        with self.connPool.connection() as conn:
            result = self.isTableExist(conn, tableName)
            getQueue.put(result)

    def dropTable(self, X):
        (_, tableName) = X
        with self.connPool.connection() as conn:
            if self.isTableExist(conn, tableName):
                conn.execute(f"DROP TABLE {tableName};")
                self.log.V("drop table:", tableName)
                return True
            else:
                self.log.D("table", tableName, "not exists.")
                return False

    def execSQL(self, X):
        (_, sql) = X
        with self.connPool.connection() as conn:
            self.log.V("exec:", sql)
            conn.execute(sql)

    def set(self, X):
        #try:
        (_, tableName, keyField, item, callback) = X
        with self.connPool.connection() as conn:
            listK = []
            listV = []
            listA = []
            listU = []
            for k in item:
                listK.append(str(k))
                listU.append(str(k)+'=excluded.'+str(k))
                if isinstance(item[k], str):
                    v = item[k].replace('\0','')
                    listV.append(v)
                else:
                    listV.append(item[k])
                listA.append("%s")
            sql = "INSERT INTO "+tableName+"("+','.join(listK)+") VALUES ("+','.join(listA)+")"
            sql+= " ON CONFLICT ("+keyField+") DO UPDATE SET "+','.join(listU)+";"
            self.log.D(tableName+".set()", timeIt=True, bypass=True)
            #
            try:
                conn.execute(sql, listV)
            except:
                self.log.W("EXECUTE ERROR:", sql, listV)
                self.log.W("tableName", tableName, "keyField", keyField, "item:", item, callback)
                traceback.print_exc()
                os._exit(1)
            #self.log.D("XXX2")
        if callback:
            callback(item[keyField])
        self.log.D(tableName+".set()", timeIt=True, minGap=7)
        #except:
        #    traceback.print_exc()
        #    os._exit(1)

    def get(self, X):
        (_, getQueue, tableName, keyField, kv) = X
        sql = "SELECT * from "+tableName+" where "+keyField+"='"+str(kv)+"'"
        with self.connPool.connection() as conn:
            self.log.D(tableName+".get()", timeIt=True, bypass=True)
            cur = conn.cursor(row_factory=psycopg.rows.dict_row)
            row = cur.execute(sql).fetchone()
            if row:
                getQueue.put(dict(row))
            else:
                getQueue.put(None)
            self.log.D(tableName+".get()", timeIt=True, minGap=7)
        return None

    def count(self, X):
        (_, getQueue, tableName) = X
        with self.connPool.connection() as conn:
            sql = f"SELECT n_live_tup from pg_stat_user_tables where relname='{tableName}';"
            row = conn.execute(sql).fetchone()
            if row:
                getQueue.put(row[0])
            else:
                getQueue.put(0)
            return None

    def query(self, X):
        (_, getQueue, tableName, keyField, sql, *args) = X
        with self.connPool.connection() as conn:
            cur = conn.cursor(row_factory=psycopg.rows.dict_row)
            rows = cur.execute(sql, *args).fetchall()
            if rows:
                result = []
                for row in rows:
                    kv = row[keyField]
                    result.append((kv, row))
                getQueue.put(result)
            else:
                getQueue.put(None)
        return None

gDbCount = 0
gDbLock = threading.Lock()
gDbWorker = None

class DbEngine:
    def __init__(self, dbName='test'):
        global gDbCount
        global gDbLock
        global gDbWorker

        self.log = Log.Log(Log.VERBOSE_LEVEL)
        self.dbName = dbName
        self.getEvent = threading.Event()
        self.getQueue = queue.Queue()

        with gDbLock:
            if gDbCount == 0:
                gDbWorker = DbEngineWorker(self.dbName)
            else:
                assert gDbWorker.getDbName() == dbName
            gDbCount += 1

        return

    def __del__(self):
        global gDbCount
        global gDbLock
        global gDbWorker
        with gDbLock:
            gDbCount -= 1
            self.log.D("DBEngine __del__", self.dbName, gDbCount)
            if gDbCount == 0:
                gDbWorker.stop()
                gDbWorker = None
        return

    def dropTable(self, tableName):
        global gDbWorker
        gDbWorker.enque(('drop', tableName)) # blocking

    def execSQL(self, sql):
        global gDbWorker
        gDbWorker.enque(('exec', sql)) # blocking

    def count(self, tableName):
        global gDbWorker
        gDbWorker.enque(('count', self.getQueue, tableName))
        return self.getQueue.get() # blocking

    def set(self, tableName, keyField, item, callback=None):
        global gDbWorker
        gDbWorker.enque(('set', tableName, keyField, item, callback))

    def get(self, tableName, keyField, kv):
        global gDbWorker
        gDbWorker.enque(('get', self.getQueue, tableName, keyField, kv))
        return self.getQueue.get() # blocking

    def query(self, tableName, keyField, sql, *args):
        global gDbWorker
        gDbWorker.enque(('query', self.getQueue, tableName, keyField, sql, *args))
        return self.getQueue.get() # blocking

    def existTable(self, tableName):
        global gDbWorker
        gDbWorker.enque(('exist', self.getQueue, tableName))
        return self.getQueue.get() # blocking

def test_basic0():
    log = Log.Log()
    engine = DbEngine()
    engine.dropTable('test')
    engine.execSQL("""CREATE TABLE IF NOT EXISTS test (
            id text primary key,
            v int
            );""")
    for i in range(1000):
        item={}
        item['id'] = str(i)
        item['v'] = i
        engine.set('test', 'id', item)

def test_basic1():
    log = Log.Log()
    engine = DbEngine()
    engine.dropTable('test')
    engine.execSQL("""CREATE TABLE IF NOT EXISTS test (
            id text primary key,
            v int
            );""")
    for i in range(1000):
        item={}
        item['id'] = str(i%100)
        item['v'] = i
        engine.set('test', 'id', item)

    for i in range(100):
        x = engine.get('test', 'id', i)
        #assert x['v']%100 == i
    log.D("count", engine.count('test'))
    return

def test_basic2():
    log = Log.Log()
    engine = DbEngine()
    engine.dropTable('test')
    engine.execSQL("""CREATE TABLE IF NOT EXISTS test (
            id text primary key,
            i int,
            f float,
            t timestamp with time zone
            );""")
    for i in range(1000):
        item={}
        item['id'] = str(i%100)
        item['i'] = i
        item['f'] = i*100.0/3.14
        item['t'] = DTime.now()
        engine.set('test', 'id', item)

    for i in range(1000):
        x = engine.get('test', 'id', i)
        if x:
            assert x['i']%100 == i
            log.D("get time", x['t'], minGap=1)
    assert engine.count('test') == 100

def test_basic3():
    log = Log.Log()
    engine = DbEngine()
    engine.dropTable('test')
    assert engine.existTable('test') == False
    engine.execSQL("""CREATE TABLE IF NOT EXISTS test (
            id int primary key,
            i int,
            f float,
            t timestamp with time zone
            );""")
    for i in range(1000):
        item={}
        item['id'] = i%100
        item['i'] = i
        item['f'] = i*100.0/3.14
        item['t'] = DTime.now()
        engine.set('test', 'id', item)

    rows = engine.query('test', 'id', 'SELECT * from test where id >= %s order by id', (30,))
    assert len(rows) == 70


if __name__ == "__main__":
    test_basic0()
    test_basic1()
    test_basic2()
    test_basic3()
