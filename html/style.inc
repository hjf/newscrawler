<meta name="viewport" content="width=360, initial-scale=1">
<meta charset="UTF-8">
<style>
body {
    margin: 0;
    padding: 0;
    background-color: #171b36;
    position: relative;
    font-family: 'Roboto', sans-serif;
    color: #777777;
    font-size: 1em;
}
.menu a {
    color: #febd09;
    font-size: 1em;
}
.menu a:active {
    color: #febd09;
    font-size: 1em;
}
.htable table {
    border-collapse: collapse;
    width: 100%;
}
.htable th {
    color: #bbbbbb;
}
.htable td {
    border-style:solid;
    border-top:none;
    border-left:none;
    border-right:none;
    border-bottom:1px solid #febd09;
    padding: 3px;
}
.htable a {
    color: #aaaaaa;
}
.htable a:active {
    color: #aaaaaa;
}

</style>
<!-- red #DF5060; -->
<!-- yellow febd09 -->
