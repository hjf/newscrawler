import DictDB

class GnewsDBPost(DictDB.DictDB):

    def __init__(self,
                 dbName         = "gnews",
                 tableName      = "post",
                 keyField       = "pid",
                 drop           = False,
                 minCache       = 7,
                 maxCache       = 0,
                 ioCache        = 777,
                 maxMemPercent  = 77.7):
        super().__init__(dbName = dbName, tableName=tableName, keyField=keyField, drop=drop, minCache=minCache,
                        maxCache=maxCache, ioCache=ioCache, maxMemPercent=maxMemPercent)

    def createTable(self):
        self.rawDB.execSQL("""CREATE TABLE IF NOT EXISTS {} (
            pid text primary key,
            cdate timestamp with time zone,
            txt text,
            main text,
            htgs text,
            utgs text,
            cats text,
            previmg text,
            ttl text,
            dsc text,
            uid text,
            txt_lang text,
            vfpst int DEFAULT 0,
            lkbpst int DEFAULT 0,
            cm int DEFAULT 0,
            shbpst int DEFAULT 0
            );""".format(self.tableName))

if __name__ == '__main__':
    db = GnewsDBPost(drop=True)
    db = None
