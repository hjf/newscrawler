import DictDB

class gettrReportDB(DictDB.DictDB):
    def __init__(self,  dbConnection, keyField, dbName = "report", dbTable = "default",
                 ioCache = 777, maxCache = 7777777, logLevel = ut.DEBUG_LEVEL):
        super().__init__(dbConnection, dbName = dbName, dbTable = dbTable, keyField = keyField,
                         ioCache = ioCache, maxCache = maxCache,
                         logLevel = logLevel,
                         drop = True)
        self.create_index(keyField, unique = True)

if __name__ == '__main__':
    pass
