import GettrDBAccount
import GettrDBPost
import Spider

class GettrSpider(Spider.Spider):
    def getDB(self, tableName):
        with self.dbPoolLock:
            if tableName not in self.dbPool:
                if tableName == 'account':
                    self.dbPool[tableName] = GettrDBAccount.GettrDBAccount(drop=False)
                elif tableName == 'post':
                    self.dbPool[tableName] = GettrDBPost.GettrDBPost(drop=False)
                else:
                    self.log.W("unknown DB", tableName)
            return self.dbPool[tableName]

