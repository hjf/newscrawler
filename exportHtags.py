import ut
import dbConnection
import pymongo
import os.path
import os

class exportHTags(ut.ut):
    def generateCSV(self, colName):
        rootFolder = 'html/export/'
        csvName = rootFolder + colName+'.csv'
        csvGzipName = csvName+'.gz'
        htmlGzipName = 'export/'+colName+'.csv.gz'
        if os.path.exists(csvGzipName):
            self.D(csvGzipName, "existed, ignore export")
        else:
            csvArgs = ' --type=csv --fields pid,cdate,udate,uid,txt,txt_en,txt_lang,lkbpst,cm,shbpst,htgs,utgs,gettr_follower,max_viewers,max_views'
            cmd = '/usr/bin/mongoexport -d report -c '+colName+' -o '+csvName + csvArgs
            self.D("Execute",cmd)
            os.system(cmd)
            cmd = '/usr/bin/gzip '+csvName
            self.D("Execute",cmd)
            os.system(cmd)

        MB = os.path.getsize(csvGzipName) / 1024.0 / 1024.0
        return (htmlGzipName, MB)


    def genReportInc(self, out):
        with open('html/report.inc','w') as f:
            print("<table class='htable' width='100%'><tr><th>Name<th>Size<th>Rows", file=f)
            for o in out:
                htagHtml = '/statistics/'+o[0]+'_htag.html'
                detailHtml = '/statistics/'+o[0]+'.html'
                print("<tr><td>", o[0], "<br> <a href='"+o[1]+"'>[download]</a>",
                      "<a href='"+htagHtml+"'>[htag]</a>",
                      #"<a href='"+detailHtml+"'>[detail]</a>",
                      "<td align=right>", f'{o[2]:.2f}MB', "<td align=right>", o[3], file=f)
            print("</table>", file=f)
            f.close()

    def htagsHtml(self, colName):
        return ['html/statistics/htags/', '/statistics/htags/', colName +'.html']

    def removeOutOfDate(self, cols):
        allowList = []

        for colName in cols:
            (folder, htmlFolder, fileName) = self.htagsHtml(colName)
            allowList.append(fileName)

        for f in os.listdir(folder):
            if f in allowList:
                continue
            self.D(f, "out of dated!")
            os.remove(folder+f)

    def run(self):
        self.connection = dbConnection.dbConnection(connect = True, logLevel = ut.DEBUG_LEVEL)
        self.D(self.connection)
        self.db = self.connection['report']
        self.D(self.db)
        cols = self.db.list_collection_names()
        cols.sort(reverse=True)
        self.removeOutOfDate(cols)
        for colName in cols:
            pass
        return

        MaxKeepCount = 7 * 24  # 7 days
        out = []
        for colName in cols:
            col = self.db[colName]
            if len(out) >= MaxKeepCount:
                self.D("Erase ", colName)
                col.drop()
                continue

            dataCount = col.count_documents({})

            (gzipName, MB) = self.generateCSV(colName)

            out.append([colName, gzipName, MB, dataCount])
            self.D(colName, MB, dataCount)

        self.genReportInc(out)

if __name__ == "__main__":
    t = exportHTags()
    t.run()
