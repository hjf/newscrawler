import Scrat
import SpiderQueen
import GnewsSuggestUser
import GnewsSuggestPost
import GnewsHeadline
import GnewsTrend
import GnewsUserPost
import GnewsAccount
import GnewsPost


def main():
    spiderQueen = SpiderQueen.SpiderQueen("queen")
    spiderQueen.register()
    spiderQueen.register( 1, GnewsSuggestUser.GnewsSuggestUser("suggu", commander=False))
    spiderQueen.register( 1, GnewsSuggestPost.GnewsSuggestPost("suggp", commander=False))
    spiderQueen.register( 1, GnewsHeadline.GnewsHeadline("headl", commander=False))
    spiderQueen.register( 1, GnewsTrend.GnewsTrend("trend", commander=False))
    spiderQueen.register( 1, GnewsUserPost.GnewsUserPost("userp", commander=False))
    spiderQueen.register( 1, GnewsPost.GnewsPost("post"))
    spiderQueen.register( 1, GnewsAccount.GnewsAccount("accou"))
    spiderQueen.waitRequestPrepare()
    Scrat.run_forever(spiderQueen)
    spiderQueen.log = None
    spiderQueen = None

if __name__ == '__main__':
    main()
