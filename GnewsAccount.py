import GnewsSpider
import scrapy
import DTime
import json
import Log
import traceback

class GnewsAccount(GnewsSpider.GnewsSpider):
    def start(self):
        accountDB = self.getDB('account')
        self.log.setTextColor(Log.COLOR256_200)

    def handleClose(self):
        pass

    def handle(self, X):
        if X[0] == 'query':
            uid = X[1]
            meta={'handle_httpstatus_all': True, 'uid':uid} # account may not exist
            url = "https://api.gettr.com/s/uinf/"+uid
            self.sendQueenRequest(url, 'accou', meta=meta)
            return

        if X[0] == 'response':
            self.handleAccount(X[1])
            return

        if X[0] == 'handleData':
            self.handleData(X[1])
            return

        self.log.W("unknown", X)

    def handleAccount(self, response):
        accountDB = self.getDB('account')
        meta = response.meta
        if response.status == 400:
            item = {"uid": meta["uid"]}
            item["status"] = "deleted"
            item["crawler_time"] = DTime.now()
            self.log.W("deleted user", item["uid"])
            accountDB.set(item)
            return

        if response.status == 520:
            self.log.W("unknown error", response.url)
            item = {"uid": meta["uid"]}
            item["crawler_time"] = DTime.now()
            accountDB.set(item)
            return
        try:
            j = json.loads(response.text)
        except json.decoder.JSONDecodeError:
            print("json error url:", response.url, "status:", response.status, "headers:", response.headers, "body:", response.body, "text:", response.text)
            traceback.print_exc()
            os._exit(1)

        data = j["result"]["data"]
        self.extractAccount(accountDB, data)

    def handleData(self, X):
        j = json.loads(X.text)
        try:
            userdata = j["result"]["aux"]["uinf"]
            accountDB = self.getDB('account')
            for user in userdata:
                self.extractAccount(accountDB, userdata[user])
        except:
            traceback.print_exc()
            pass


    def extractAccount(self, accountDB, data):
        item = {}
        self.extractStr(item, "uid", data, "_id")
        oldItem = accountDB.get(item["uid"])
        if not oldItem:
            userStr = "[New user]"
            item["crawler_time"] = DTime.now()
        else:
            userStr = "[Update user]"
            item["crawler_time"] = oldItem["crawler_time"]

        self.extractStr(item, "nickname", data, "nickname")
        self.extractStr(item, "ousername", data, "ousername")
        self.extractStr(item, "status", data, "status")
        self.extractDatetime(item, "cdate", data, "cdate")
        self.extractStr(item, "lang", data, "lang")
        self.extractInt(item, "infl", data, "infl")
        self.extractStr(item, "ico", data, "ico")
        self.extractInt(item, "flw", data, "flw")
        self.extractInt(item, "flg", data, "flg")
        self.extractInt(item, "lkspst", data, "lkspst")
        self.extractInt(item, "lkscm", data, "lkscm")
        self.extractInt(item, "vfpst", data, "vfpst")
        self.extractInt(item, "pspst", data, "pspst")

        if accountDB.set(item):
            self.log.D(userStr, item["uid"], minGap = 7, timeIt = True)

if __name__ == '__main__':
    account = SpiderPost('account', "[Account]")
    account.stop()
    account = None

