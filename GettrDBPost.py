import DictDB

class GettrDBPost(DictDB.DictDB):

    def __init__(self,
                 dbName         = "gettr",
                 tableName      = "post",
                 keyField       = "pid",
                 drop           = False,
                 minCache       = 7,
                 maxCache       = 0,
                 ioCache        = 777,
                 maxMemPercent  = 77.7):
        super().__init__(dbName = dbName, tableName=tableName, keyField=keyField, drop=drop, minCache=minCache,
                        maxCache=maxCache, ioCache=ioCache, maxMemPercent=maxMemPercent)

    def createTable(self):
        self.rawDB.execSQL("""CREATE TABLE IF NOT EXISTS {} (
            pid text primary key,
            cdate timestamp with time zone,
            udate timestamp with time zone,
            uid text,
            txt text,
            txt_en text,
            txt_zhs text,
            txt_zht text,
            txt_lang text,
            prevsrc text,
            previmg text,
            prev_lang text,
            ttl text,
            dsc text,
            vid text,
            vid_dur int DEFAULT 0,
            vid_wid int DEFAULT 0,
            vid_hgt int DEFAULT 0,
            lkbpst int DEFAULT 0,
            cm int DEFAULT 0,
            shbpst int DEFAULT 0,
            htgs text,
            utgs text,
            max_views int DEFAULT 0
            );""".format(self.tableName))

if __name__ == '__main__':
    db = GettrDBPost()
    db = None
