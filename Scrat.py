import scrapy
import scrapy.crawler
import Log
import SpiderQueen
import os
import traceback
import threading
import time
import random

class Scrat(scrapy.Spider):
    name = "Scrat"
    def __init__(self):
        self.name = Scrat.name
        self.log = Log.Log(who='scrat')
        self.log.setTextColor(Log.COLOR256_196)
        #self.log.setLogLevel(Log.VERBOSE_LEVEL)
        self.log.D("constructor")
        self.inFlightCountLock = threading.Lock()
        self.inFlightCount = 0
        self.loadUserAgentList()

    def closed(self, reason):
        self.log.D("closed due to",reason)
        self.log = None  # must release resource at closed()
        self.spiderQueen.stop()
        self.spiderQueen = None # must release resource at closed()

    def __del__(self):
        pass

    def loadUserAgentList(self):
        with open("useragent.txt") as file:
            lines = [line.rstrip() for line in file]
            self.uaList = lines
            self.log.D("Loading user agent list", len(self.uaList))

    def request_reached_downloader(self, request, spider):
        with self.inFlightCountLock:
            self.inFlightCount += 1
            #self.log.W("reach_down", self.inFlightCount)

    def request_left_downloader(self, request, spider):
        with self.inFlightCountLock:
            self.inFlightCount -= 1
            #self.log.W("left_down", self.inFlightCount)

    def errback(self, failure):
        self.log.W("[errback]", failure) # hjf
        url = failure.value.response.url
        meta = failure.value.response.meta
        target = meta['target']
        if "retryCnt" not in meta:
            meta["retryCnt"] = 0
        meta["retryCnt"] += 1
        self.log.W("[errback] retry=",meta["retryCnt"], url, failure )
        if meta["retryCnt"] <= 3:
            #self.spiderQueen.enque('queen', ('request', url, meta))
            self.spiderQueen.sendQueenRequest(url, target,  meta=meta)

    def submitRequest(self, X):
        #ua = random.choice(self.uaList)
        #self.log.D(ua)
        meta = X[2]
        headers = X[3]
        cookies = X[4]
        if 'User-Agent' not in headers:
            ua="Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:98.0) Gecko/20100101 Firefox/98.0"
            headers['User-Agent'] = ua

        return scrapy.Request(X[1], meta=meta, dont_filter=True, errback=self.errback, headers=headers, cookies=cookies)

    def start_requests(self):
        X = self.spiderQueen.pollRequest()
        yield self.submitRequest(X)

    def parse(self, response):
        self.log.D("[scrapy]", self.inFlightCount, response.url, minGap=10, timeIt=True)
        try:
            self.spiderQueen.handleResponse(response)
            submitCnt=0
            while True:
                with self.inFlightCountLock:
                    cnt = self.inFlightCount
                X = self.spiderQueen.pollRequest(False, cnt)
                if X:
                    submitCnt +=1
                    #self.log.V("yield request", X)
                    yield self.submitRequest(X)

                    # 1 response only can generate 2 request to smooth submit throughput
                    #if cnt < self.spiderQueen.getMaxScrapyThread() and submitCnt<2:
                    #    continue # continue submit

                    if cnt+submitCnt < self.spiderQueen.getMaxScrapyThread():
                        continue # continue submit

                if submitCnt>0:
                    break

                with self.inFlightCountLock:
                    if self.inFlightCount > 0:
                        break


                if self.spiderQueen.isExit():
                    self.log.D("Leave parse() due to queen.isExit()")
                    break

                cmder = self.spiderQueen.getSpiderCommanderList()
                qcnt = self.spiderQueen.getGlobalQueueSize()
                if  len(cmder) > 0 or qcnt>0:
                    self.log.D("SpiderCommander=["+','.join(cmder)+"] GlobalQueueSize=", qcnt, "submitCnt=", submitCnt, "inFlightCount=", self.inFlightCount)
                    time.sleep(3)
                else:
                    self.log.W("No spider commander exists. Leave parse()")
                    break
        except:
            traceback.print_exc()
            os._exit(1)

    @classmethod
    def from_crawler(cls, crawler, *args, spiderQueen = None, **kwargs):
        try:
            spider = cls(*args, **kwargs)
            spider._set_crawler(crawler)
            spider.spiderQueen = spiderQueen
            #crawler.signals.connect(spider.request_dropped, signal=scrapy.signals.request_dropped)
            crawler.signals.connect(spider.request_left_downloader, signal=scrapy.signals.request_left_downloader)
            crawler.signals.connect(spider.request_reached_downloader, signal=scrapy.signals.request_reached_downloader)

            return spider
        except:
            traceback.print_exc()
            os._exit(1)


def run_forever(spiderQueen):
    log = Log.Log(who='Scrat')
    process = scrapy.crawler.CrawlerProcess({
        #"TWISTED_REACTOR" : 'twisted.internet.asyncioreactor.AsyncioSelectorReactor',
        "LOG_ENABLED" : True,
        "LOG_FILE" : "log/scrapy.log",
        #"LOG_LEVEL" : "DEBUG",
        "LOG_LEVEL" : "WARNING",
        "CONCURRENT_REQUESTS" : spiderQueen.getMaxScrapyThread(),
        })
    process.crawl(Scrat, spiderQueen=spiderQueen)
    log.D("process start")
    process.start() # the script will block here until the crawling is finished
    log.D("process stop")
    process.stop()
    log.D("process terminate")
    #process = None          # must release resource
    log = None              # must release resource
    #spiderQueen = None      # must release resource

#def main():
#    Scrat.run_forever(None)

#if __name__ == '__main__':
#    main()
