import ut
import dbConnection
import pymongo
import os.path
import os
import dt

class exportAccount(ut.ut):
    def generateCSV(self, name):
        rootFolder = 'html/export/'
        csvName = rootFolder + name+'.csv'
        csvGzipName = csvName+'.gz'
        htmlGzipName = 'export/'+name+'.csv.gz'
        if os.path.exists(csvGzipName):
            self.D(csvGzipName, "existed, ignore export")
        else:
            csvArgs = ' --type=csv --fields uid,status,gettr_follower,gettr_following,create_date,infl,lkspst,lkscm,shspst,twitter_follower,twitter_following'
            cmd = '/usr/bin/mongoexport -d gettr -c account -o '+csvName + csvArgs
            self.D("Execute",cmd)
            os.system(cmd)
            cmd = '/usr/bin/gzip -f '+csvName
            self.D("Execute",cmd)
            os.system(cmd)

        MB = os.path.getsize(csvGzipName) / 1024.0 / 1024.0
        return (htmlGzipName, MB)

    def removeCSV(self):
        for f in os.listdir('html/export'):
            if f[-7:] == '.csv.gz' and f[:7] == 'account':
                self.D("remove old account html/export/"+f)
                os.remove('html/export/'+f)
                

    def genReportInc(self, out):
        with open('html/account.inc','w') as f:
            print("<table class='htable' width='100%'><tr><th>Name<th>Size<th>Rows", file=f)
            print("<tr><td><a href='"+out[1]+"'>",out[0],"</a><td align=right>",f'{out[2]:.2f}MB',"<td align=right>",out[3], file=f)
            print("</table>", file=f)
            f.close()

    def run(self):
        self.connection = dbConnection.dbConnection(connect = True, logLevel = ut.DEBUG_LEVEL)
        self.D(self.connection)
        self.db = self.connection['gettr']
        self.D(self.db)
        col = self.db['account']

        dataCount = col.count_documents({})
        name = dt.now().strftime("account%Y%m%d_%H%M")

        self.removeCSV()
        (gzipName, MB) = self.generateCSV(name)
        out = [name, gzipName, MB, dataCount]
        self.D(out)
        self.genReportInc(out)

if __name__ == "__main__":
    t = exportAccount()
    t.run()
