import DictDB

class GnewsDBAccount(DictDB.DictDB):

    def __init__(self,
                 dbName         = "gnews",
                 tableName      = "account",
                 keyField       = "uid",
                 drop           = False,
                 minCache       = 7,
                 maxCache       = 0,
                 ioCache        = 77,
                 maxMemPercent  = 77.7):
        super().__init__(dbName = dbName, tableName=tableName, keyField=keyField, drop=drop, minCache=minCache,
                        maxCache=maxCache, ioCache=ioCache, maxMemPercent=maxMemPercent)

    def createTable(self):
        self.rawDB.execSQL("""CREATE TABLE IF NOT EXISTS {} (
            uid text primary key,
            nickname text,
            ousername text,
            status text,
            cdate timestamp with time zone,
            lang text,
            infl int DEFAULT 0,
            ico text,
            flw int DEFAULT 0,
            flg int DEFAULT 0,
            lkspst int DEFAULT 0,
            lkscm int DEFAULT 0,
            vfpst int DEFAULT 0,
            pspst int DEFAULT 0,
            crawler_time timestamp with time zone,
            crawler_post_time timestamp with time zone
            );
            """.format(self.tableName))

if __name__ == '__main__':
    db = GnewsDBAccount(drop=True)
    db = None
