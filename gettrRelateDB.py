import DictDB

class gettrRelateDB(DictDB.DictDB):
    def __init__(self, dbConnection, dbName = "gettr", dbTable = "relate"):
        super().__init__(dbConnection, dbName = dbName, dbTable = dbTable, keyField = "uid",
                         ioCache = 7777, maxCache = 7777777,
                         logLevel = ut.DEBUG_LEVEL)
        self.create_index("uid", unique = True)

if __name__ == '__main__':
    pass
