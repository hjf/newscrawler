import argparse
import dbConnection
import dt
import os
import os.path
import pymongo
import ut

def genName(item, url=False, gzip=False, baseName=False, folderName=False):
    folder = item[1] if url else item[0]
    postfix = '.gz' if gzip else ''
    if baseName:
        return item[2]+postfix
    if folderName:
        return folder
    return folder + item[2] + postfix

def linkHtml(colName, url=False, gzip=False, baseName=False, folderName=False, lang="", sortKey="", page=0):
    if sortKey != "":
        colName += '.'+sortKey
    if lang != "":
        colName += '.'+lang
    if page>0:
        colName += '.'+str(page)
    fileName = colName +'.html'
    item = ['html/statistics/link/', '/statistics/link/', fileName]

    return genName(item, url, gzip, baseName, folderName)

def linkCsv(colName, url=False, gzip=False, baseName=False, folderName=False, lang=""):
    if lang != "":
        colName += '.'+lang
    item = ['html/statistics/link/', '/statistics/link/', colName +'.csv']
    return genName(item, url, gzip, baseName, folderName)

def postCsv(colName, url=False, gzip=False, baseName=False, folderName=False, lang=""):
    if lang != "":
        colName += '.'+lang
    item = ['html/export/', '/export/', colName +'.csv']
    return genName(item, url, gzip)


class exportReport(ut.ut):
    def generateCSV(self, colName, skipCsv):
        if skipCsv:
            self.D("skipcsv!", postCsv(colName, gzip=True), " export")
        elif os.path.exists(postCsv(colName, gzip=True)):
            self.D(postCsv(colName, gzip=True), "existed, ignore export")
        else:
            csvArgs = ' --type=csv --fields pid,cdate,udate,uid,txt,txt_lang,prevsrc,previmg,ttl,dsc,prev_lang'
            csvArgs+= ',lkbpst,cm,shbpst,htgs,utgs,gettr_follower,max_views,max_viewers,txt_en,txt_zhs,txt_zht'
            csvArgs+= ',vid,vid_dur,vid_wid,vid_hgt'

            cmd = '/usr/bin/mongoexport -d report -c '+colName+' -o '+postCsv(colName) + csvArgs
            self.D("Execute",cmd)
            os.system(cmd)
            cmd = '/usr/bin/gzip -f '+postCsv(colName)
            self.D("Execute",cmd)
            os.system(cmd)

        if os.path.exists(postCsv(colName, gzip=True)):
            MB = os.path.getsize(postCsv(colName, gzip=True)) / 1024.0 / 1024.0
        else:
            MB = 0
        return MB

    def removeCSV(self, cols):
        for f in os.listdir('html/export'):
            if f[-7:] == '.csv.gz' and f[:-7] in cols:
                self.D(f, "exists in database")
            else:
                self.D(f, "not exists in database, removed!")
                os.remove('html/export/'+f)
    def strTimeFromColName(self, colName):
        t = ""
        if colName.find('post') == 0:
            y = int(colName[4:][:4])
            m = int(colName[8:][:2])
            d = int(colName[10:][:2])
            H = int(colName[13:][:2])
            M = int(colName[15:][:2])
            tmp = dt.ymdhm(y,m,d,H,M)

            t += '<br><i>盤古:' + dt.to_taipei(tmp).strftime("%Y/%m/%d %H:%M")+'</i>'
            t += '<br><i>美東:' + dt.to_us(tmp).strftime("%Y/%m/%d %H:%M")+'</i>'
        return t
    def getStrFromFile(self, fileName):
        s = ""
        with open(fileName,'r') as mf:
            s = '\n'.join(mf.readlines())
            mf.close()
        return s

    def genReportHtml(self, out):
        with open('html/report.html','w') as f:
            print("""<html>
            <head>
            <meta name="viewport" content="width=360, initial-scale=1">
            <meta charset="UTF-8">
            <title>MOS News task force</title>
            <link rel="stylesheet" href="/report.css">
            </head>
            <body>""", self.getStrFromFile('html/menu.inc'), file=f)
            print("<table class='htable' width='100%'>", file=f)
            for o in out:
                colName = o[0]
                MB = o[1]
                dataCount = o[2]
                #detailHtml = '/statistics/'+colName+'.html'
                print("<tr><td><font color='#febd09'>", colName,"</font>",
                      "<br><table><tr><td>[Posts/24小時貼文]<td><a href='"+postCsv(colName, gzip=True, url=True)+"'>csv</a>",
                      "<td align=right>", f'{MB:.2f}MB', "<td align=right>", dataCount, "篇"
                      "</table>",
                      "<br><table><tr><td>[Links/24小時鏈結]<td>", "<a href='"+linkCsv(colName, gzip=True, url=True)+"'>csv</a>",
                      "<tr><td><td>","<a href='"+linkHtml(colName, url=True, sortKey='repost')+"'>完整版-按 repost 排序</a>",
                      "<tr><td><td>","<a href='"+linkHtml(colName, url=True, sortKey='max_views')+"'>完整版-按 max_views 排序</a>",
                      "<tr><td><td>","<a href='"+linkHtml(colName, url=True, sortKey='cdate')+"'>完整版-按 cdate 排序</a>",
                      "<tr><td><td>","<a href='"+linkHtml(colName, url=True,lang="zh", sortKey='repost')+"'>中文子集合-按 repost 排序</a>",
                      "<tr><td><td>","<a href='"+linkHtml(colName, url=True,lang="zh", sortKey='max_views')+"'>中文子集合-按 max_views 排序</a>",
                      "<tr><td><td>","<a href='"+linkHtml(colName, url=True,lang="zh", sortKey='cdate')+"'>中文子集合-按 cdate 排序</a>",
                      "</table>",
                      "<br>",self.strTimeFromColName(colName),
                       file=f)
            print("</table></body></html>", file=f)
            f.close()

    def run(self, skipCsv):
        self.connection = dbConnection.dbConnection(connect = True, logLevel = ut.DEBUG_LEVEL)
        self.D(self.connection)
        self.db = self.connection['report']
        self.D(self.db)
        cols = self.db.list_collection_names()
        cols.sort(reverse=True)
        self.removeCSV(cols)

        MaxKeepCount = 7 * 24  # 7 days
        out = []
        for colName in cols:
            col = self.db[colName]
            if len(out) >= MaxKeepCount:
                self.D("Erase ", colName)
                col.drop()
                continue

            dataCount = -1

            dataCount = col.estimated_document_count()

            MB = self.generateCSV(colName, skipCsv)

            out.append([colName, MB, dataCount])
            self.D(colName, MB, dataCount)

        self.genReportHtml(out)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Export post html')
    parser.add_argument('--skipcsv', action='store_true', default=False,
                        help='reprocess all database')
    args = parser.parse_args()
    t = exportReport()
    t.run(args.skipcsv)
