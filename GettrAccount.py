import GettrSpider
import scrapy
import DTime
import json
import Log

class GettrAccount(GettrSpider.GettrSpider):
    def start(self):
        accountDB = self.getDB('account')
        self.log.setTextColor(Log.COLOR256_200)

    def handleClose(self):
        pass

    def handle(self, X):
        if X[0] == 'query':
            uid = X[1]
            meta={'handle_httpstatus_all': True, 'uid':uid} # account may not exist
            url = "https://api.gettr.com/s/uinf/"+uid
            self.sendQueenRequest(url, 'accou', meta=meta)
            return

        if X[0] == 'response':
            self.handleAccount(X[1])
            return

        if X[0] == 'handleFollowData':
            self.handleFollowData(X[1])
            return

        self.log.W("unknown", X)

    def handleAccount(self, response):
        accountDB = self.getDB('account')
        meta = response.meta
        if response.status == 400:
            item = {"uid": meta["uid"]}
            item["status"] = "deleted"
            item["crawler_time"] = DTime.now()
            self.log.W("deleted user", item["uid"])
            accountDB.set(item)
            return

        if response.status == 520:
            self.log.W("unknown error", response.url)
            item = {"uid": meta["uid"]}
            item["crawler_time"] = DTime.now()
            accountDB.set(item)
            return
        try:
            j = json.loads(response.text)
        except json.decoder.JSONDecodeError:
            print("json error url:", response.url, "status:", response.status, "headers:", response.headers, "body:", response.body, "text:", response.text)
            traceback.print_exc()
            os._exit(1)

        data = j["result"]["data"]
        self.extractAccount(accountDB, data)

    def handleFollowData(self, X):
        j = json.loads(X.text)
        try:
            userdata = j["result"]["aux"]["uinf"]
            accountDB = self.getDB('account')
            for user in userdata:
                self.extractAccount(accountDB, userdata[user])
        except:
            pass


    def extractAccount(self, accountDB, data):
        item = {}
        self.extractStr(item, "uid", data, "_id")
        oldItem = accountDB.get(item["uid"])
        if not oldItem:
            userStr = "[New user]"
        else:
            userStr = "[Update user]"

        self.extractStr(item, "status", data, "status")
        self.extractStr(item, "location", data, "location")
        self.extractStr(item, "lang", data, "lang")
        self.extractInt(item, "infl", data, "infl")

        self.extractInt(item, "twitter_follower", data, "twt_flg")
        self.extractInt(item, "twitter_following", data, "twt_flw")
        self.extractInt(item, "gettr_follower", data, "flg")
        self.extractInt(item, "gettr_following", data, "flw")
        self.extractDatetime(item, "cdate", data, "cdate")

        if not oldItem or (DTime.now() - oldItem["crawler_time"]).days >= 7:
            self.extractInt(item, "lkspst", data, "lkspst")
            self.extractInt(item, "lkscm", data, "lkscm")
            self.extractInt(item, "shspst", data, "shspst")
            item["crawler_time"] = DTime.now()
            if oldItem:
                userStr = "[Update user all]"

        if accountDB.set(item):
            self.log.D(userStr, item["uid"], minGap = 7, timeIt = True)

if __name__ == '__main__':
    account = SpiderPost('account', "[Account]")
    account.stop()
    account = None

