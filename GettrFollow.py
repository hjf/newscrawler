import GettrSpider
import scrapy
import DTime
import json
import Log

class GettrFollow(GettrSpider.GettrSpider):
    def start(self):
        self.log.setTextColor(Log.COLOR256_112)
        self.enque('follo', ('scanAccountFollow', DTime.now()))

    def handle(self, X):
        if X[0] == 'requestQueen':
            url = X[1]
            meta = X[2]
            self.sendQueenRequest(url, 'follo', meta=meta)
            return

        if X[0] == 'response':
            self.handleFollow(X[1])
            return

        if X[0] == 'scanAccountFollow':
            self.scanAccountFollow(maxTime = X[1])
            return

        self.log.W("unknown", X)

    def handleClose(self):
        pass

    def scanAccountFollow(self, maxTime):
        if maxTime == None:
            maxTime = DTime.now()
        accountDB = self.getDB('account')
        X = accountDB.query("""select * from account
            where (crawler_follow_time < %s or crawler_follow_time is null) and (status != 'deleted')
            order by crawler_follow_time limit 7""", (maxTime,))
        #self.log.D("scanAccountFollow maxTime", maxTime)
        updateCnt = 0
        if X:
            cnt=0
            for uid in X:
                cnt += 1
                offset = 0
                item = accountDB.get(uid)
                minTime = item['crawler_follow_time']
                if minTime == None:
                    minTime = DTime.now_delta(days=-1)

                if minTime < maxTime  and item["status"] != "deleted":
                    self.log.D("scanAccountFollow:", cnt, "/", len(X), "uid:", uid, "minTime:", minTime, "maxTime:", maxTime, timeIt=True, minGap=7)
                    # update
                    item['crawler_follow_time'] = maxTime
                    accountDB.set(item)
                    updateCnt += 1
                    self.scanAccountFollowing(uid, 0, maxTime, timeout=1)
                    self.scanAccountFollower(uid, 0, maxTime, timeout=1)

        accountDB.flush()
        if updateCnt>0:
            self.enque('follo', ('scanAccountFollow', maxTime), timeout=77)
        else:
            self.enque('follo', ('scanAccountFollow', None), timeout=77)


    def scanAccountFollowing(self, uid, offset, maxTime, timeout=0):
        meta={'uid':uid, 'offset':offset, 'type':'following', 'maxTime': maxTime, 'handle_httpstatus_all': True}
        url = f"https://api.gettr.com/u/user/{uid}/followings/?offset={offset}&max=20&incl=userstats|userinfo|followings"
        self.enque('follo', ('requestQueen', url, meta), timeout=timeout)

    def scanAccountFollower(self, uid, offset, maxTime, timeout=0):
        meta={'uid':uid, 'offset':offset, 'type':'follower', 'maxTime': maxTime, 'handle_httpstatus_all': True}
        url = f"https://api.gettr.com/u/user/{uid}/followers/?offset={offset}&max=20&incl=userstats|userinfo|followings"
        self.enque('follo', ('requestQueen', url, meta), timeout=timeout)

    def handleFollow(self, X):
        accountDB = self.getDB('account')
        meta = X.meta
        queryUid = meta['uid']
        queryUser = accountDB.get(queryUid)

        if meta["type"] == "follower":
            flr = queryUser["gettr_follower"] if "gettr_follower" in queryUser else "N/A"
        else:
            flr = queryUser["gettr_following"] if "gettr_following" in queryUser else "N/A"
        self.log.D("[follow]", queryUid, meta["type"], meta["offset"], "/", flr, minGap=7, timeIt=True)

        if X.status == 400:
            queryUser["status"] = "deleted"
            self.log.W("deleted user", queryUser)
            accountDB.set(queryUser)
            return

        if X.status == 520:
            self.log.W("unknown error", X.url)
            return

        j = json.loads(X.text)
        lst = j["result"]["data"]["list"]

        if len(lst)>0:
            try:
                userdata = j["result"]["aux"]["uinf"]
                self.sendAccountFollowData(X)
            except:
                pass

            if X.meta['type'] == 'following':
                self.scanAccountFollowing(X.meta['uid'], X.meta['offset']+20, X.meta['maxTime'])
            else:
                self.scanAccountFollower(X.meta['uid'], X.meta['offset']+20, X.meta['maxTime'])

if __name__ == '__main__':
    pass

