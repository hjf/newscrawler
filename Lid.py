import fasttext
import threading
class Lid:
    def __init__(self, modelName = 'lid.176.bin'):
        self.lock = threading.Lock()
        fasttext.FastText.eprint = lambda x: None
        self.model = fasttext.load_model(modelName)

    def getLang(self, text):
        with self.lock:
            predictions = self.model.predict(text)
            return predictions[0][0][-2:]

if __name__ == "__main__":
    detect = Lid()
    print(detect.getLang("elf, modelName = 'lid.176.bin'):"))
    print(detect.getLang("'This text 中文輸入法'"))
    print(detect.getLang("This text 中文輸入法"+"je mange de la nourriture"))
    print(detect.getLang(""))
