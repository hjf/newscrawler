import gc
import Scrat
import Spider
import SpiderAccount
import SpiderFollow
import SpiderPost
import SpiderQueen
import SpiderTranslate
import SpiderTrend
import SpiderKing
import SpiderTwPost
import SpiderTwSearch
import sys

def main():
    spiderQueen = SpiderQueen.SpiderQueen("queen")
    spiderQueen.register()
    spiderQueen.register(  1, SpiderTwPost.SpiderTwSearch("twsch"))
    #spiderQueen.register(  1, SpiderKing.SpiderKing("king"))
    #spiderQueen.register(  1, SpiderTwPost.SpiderTwPost("twpst"))
    #spiderQueen.register( 20, SpiderTrend.SpiderTrend("trend"))
    #spiderQueen.register(  1, SpiderPost.SpiderPost("post"))
    #spiderQueen.register(  2, SpiderTranslate.SpiderTranslate("trans"))
    #spiderQueen.register(200, SpiderFollow.SpiderFollow("follo"))
    #spiderQueen.register(200, SpiderAccount.SpiderAccount("accou"))
    spiderQueen.waitRequestPrepare()
    Scrat.Scrat.run_forever(spiderQueen)
    spiderQueen = None

if __name__ == '__main__':
    main()
