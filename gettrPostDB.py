import DictDB

class gettrPostDB(DictDB.DictDB):
    def __init__(self, dbConnection, dbName = "testdb", dbTable = "post",ioCache = 7777, maxCache = 7777777, logLevel = ut.DEBUG_LEVEL, drop = False):
        super().__init__(dbConnection, dbName = dbName, dbTable = dbTable, keyField = "pid",
                         ioCache = ioCache, maxCache = maxCache,
                         logLevel = logLevel, drop = drop)
        self.create_index("pid", unique = True)
        self.create_index([("cdate", -1)])

if __name__ == '__main__':
    pass
