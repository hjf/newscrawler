import GnewsSpider
import scrapy
import DTime
import json
import Log
import traceback

class GnewsUserPost(GnewsSpider.GnewsSpider):
    def start(self):
        self.log.setTextColor(Log.COLOR256_112)
        self.enque('userp', ('scanAccountPost', DTime.now()))

    def handle(self, X):
        if X[0] == 'scanAccountPost':
            self.scanAccountPost(maxTime = X[1])
            return

        if X[0] == 'response':
            self.handlePost(X[1])
            return



        self.log.W("unknown", X)

    def handleClose(self):
        pass

    def scanAccountPost(self, maxTime):
        if maxTime == None:
            maxTime = DTime.now()
        accountDB = self.getDB('account')
        X = accountDB.query("""select * from account
            where (crawler_post_time < %s or crawler_post_time is null)
            order by crawler_post_time""", (maxTime,))
        #self.log.D("scanAccountPost maxTime", maxTime)
        updateCnt = 0
        if X:
            cnt=0
            for uid in X:
                cnt += 1
                offset = 0
                item = accountDB.get(uid)
                minTime = item['crawler_post_time']
                if minTime == None:
                    minTime = DTime.now_delta(days=-365*10) # 10 years ago
                    self.log.D("scan all", "uid:", uid, "minTime:", minTime, "maxTime:", maxTime, timeIt=True, minGap=7)

                if minTime < maxTime:
                    self.log.D("scanAccountPost:", cnt, "/", len(X), "uid:", uid, "minTime:", minTime, "maxTime:", maxTime, timeIt=True, minGap=7)
                    # update
                    item['crawler_post_time'] = maxTime
                    accountDB.set(item)
                    updateCnt += 1
                    self.scanAccountPost2(uid, 0, minTime, maxTime, timeout=1)

        accountDB.flush()
        if updateCnt>0:
            self.enque('userp', ('scanAccountPost', maxTime), timeout=77)
        #else:
        #    self.sendQueenUnregister()


    def scanAccountPost2(self, uid, offset, minTime, maxTime, timeout=0):
        meta={'uid':uid, 'offset':offset, 'minTime':minTime, 'maxTime': maxTime, 'handle_httpstatus_all': True}
        url = f"https://api.gnews.org/u/user/{uid}/posts?offset={offset}&max=20&fp=f_uo"
        self.log.D("scanAccountPost2", uid, "offset:",offset, timeIt=True, minGap=7)
        self.sendQueenRequest(url, 'userp', meta=meta)


    def handlePost(self, X):
        isUpdated = False
        try:
            postDB = self.getDB('post')

            j = json.loads(X.text)
            meta = X.meta

            if j["rc"] == "OK" and len(j["result"])==0:  # empty result
                return

            d = j["result"]["aux"]["post"]

            oldestTime = meta['maxTime']
            newestTime = None
            for pid in d:
                if not postDB.get(pid):
                    self.sendPostQuery(pid)
                    isUpdated = True

                post = d[pid]
                item = {}
                self.extractDatetime(item, "cdate", post, "cdate")
                oldestTime = min(oldestTime, item["cdate"])

                if not newestTime:
                    newestTime = item["cdate"]
                else:
                    newestTime = max(newestTime, item["cdate"])

            self.log.D("user post", meta["uid"], "offset",meta['offset'], "newestTime", newestTime, "oldestTime", oldestTime, "minTime", meta['minTime'] , "maxTime", meta['maxTime'] ,timeIt=True, minGap=7)

            if newestTime <meta['minTime']:
                return
            else: # go next page
                self.log.D("user post next page", meta["uid"], "offset",meta['offset'], "newestTime", newestTime, "oldestTime", oldestTime, "minTime", meta['minTime'] , "maxTime", meta['maxTime'] , timeIt=True, minGap=7)
                self.scanAccountPost2(meta['uid'], meta['offset']+20, meta['minTime'], meta['maxTime'], timeout=1)
        except:
            self.log.W(X.url)
            self.log.W(X.text)
            traceback.print_exc()
            pass
        return
