from thefuzz import fuzz
import argparse
import dbConnection
import dt
import gettrLinkDB
import os
import os.path
import pandas
import pymongo
import re
import ut

def genName(item, url=False, gzip=False, baseName=False, folderName=False):
    folder = item[1] if url else item[0]
    postfix = '.gz' if gzip else ''
    if baseName:
        return item[2]+postfix
    if folderName:
        return folder
    return folder + item[2] + postfix
def linkHtml(colName, url=False, gzip=False, baseName=False, folderName=False, lang="", sortKey="", page=0):
    if sortKey != "":
        colName += '.'+sortKey
    if lang != "":
        colName += '.'+lang
    if page>0:
        colName += '.'+str(page)
    fileName = colName +'.html'
    item = ['html/statistics/link/', '/statistics/link/', fileName]

    return genName(item, url, gzip, baseName, folderName)

def linkCsv(colName, url=False, gzip=False, baseName=False, folderName=False, lang=""):
    if lang != "":
        colName += '.'+lang
    item = ['html/statistics/link/', '/statistics/link/', colName +'.csv']
    return genName(item, url, gzip, baseName, folderName)

class exportLinks(ut.ut):
    def __init__(self):
        super().__init__(logLevel=ut.DEBUG_LEVEL)

    def removeOutOfDate(self, cols, lang):
        allowList = []
        for colName in cols:
            for l in lang:
                allowList.append(linkHtml(colName, baseName=True, lang=l))
                allowList.append(linkCsv(colName, gzip=True, baseName=True, lang=l))

        #self.D("allowList:", allowList)
        folderName = linkCsv(colName, folderName=True)
        for f in os.listdir(folderName):
            if f in allowList:
                continue

            # general check collection name
            found = False
            for c in cols:
                if f.find(c)>=0:
                    found = True
                    break
            if found:
                continue

            self.D(f, "out of dated!")
            os.remove(folderName+f)

    def setStr(self, item, ik, d, dk, prefix = ""):
        if dk in d:
            item[ik] = prefix + str(d[dk]).strip()
        else:
            item[ik] = ""

    def removeURL(self, s):
        return re.sub('https?://[^\s]+','',s)

    def appendFuzzyStr(self, item, ik, d, dk, prefix):
        if ik not in item:
            item[ik] = ""

        if dk in d:
            s = d[dk]
            s = self.removeURL(s).strip()

            if len(s) > 0:
                if fuzz.partial_ratio(s, item[ik]) < 60:
                    item[ik] += "<br>" + prefix+s
                    return True

        return False

    def maxInt(self, item, ik, d, dk):
        if ik not in item:
            item[ik] = 0
        if dk in d:
            item[ik] = max(int(float(d[dk])), int(float(item[ik])))

    def addInt(self, item, ik, d, dk):
        if ik not in item:
            item[ik] = 0
        if dk in d:
            item[ik] += int(float(d[dk]))

    def setOldDatetime(self, item, ik, d, dk):
        if dk in d:
            if dt.same_type(d[dk]):
                newd = dt.dt(d[dk])
            else:
                newd = dt.fromtimestamp(float(d[dk])/1000.0)
            if ik not in item:
                item[ik] = newd
            elif item[ik] > newd:
                item[ik] = newd

    def generateHTML(self, colName, links, lang, sortKey, pageSize = 1000):

        idx = list(links.keys())
        idx = sorted(idx, key = lambda x: links[x][sortKey], reverse = True)

        totalPage = int(len(idx)/pageSize)+1
        for page in range(totalPage):
            pIdx = idx[page*pageSize:(page+1)*pageSize]

            self.D("Export", linkHtml(colName, lang=lang, sortKey=sortKey, page=page))
            with open(linkHtml(colName, lang=lang, sortKey=sortKey, page=page), 'w') as f:
                print('<html><head><title>'+colName+'</title>', file=f)
                print("""
                <meta name="viewport" content="width=360, initial-scale=1">
                <meta charset="UTF-8">
                <title>MOS News task force</title>
                <link rel="stylesheet" href="/links.css">
                </head><body>""", file=f)
                print("<a href='/'>Home</a>", file=f)
                for p in range(totalPage):
                    lb = '[' if p==page else ' '
                    rb = ']' if p==page else ' '
                    print("<a href='"+linkHtml(colName, url=True, lang=lang, sortKey=sortKey, page=p)+"'>",
                            lb+str(p)+rb,"</a> ", file=f)
                print("<br><hr>", file=f)
                cnt = page*pageSize
                for k in pIdx:
                    i = links[k]
                    cnt +=1

                    videoTxt = ""
                    if len(i['vid']) > 0:
                        videoTxt = "[video exists]"
                        if i['vid_wid'] == 0:
                            videoTxt += i['vid']
                        else:
                            videoTxt += "("+str(i['vid_wid'])+"x"+str(i['vid_hgt'])+" "+str(i['vid_dur'])+"s)"

                    tmp = dt.dt(i['cdate'])
                    print("<hr>", file=f)
                    print("<p><b><a href='"+i['prevsrc'] +"'>", i['ttl'], "</a></b>", file=f)
                    m = re.search('https?://(www\.)?(.*?)/', i['prevsrc'])
                    if m:
                        print("<p><i>("+m.group(2)+")</i>", file=f)
                    print("<p>", i['dsc'], file=f)
                    print("<table><tr><td><table border=1>", file=f)
                    print("<tr><td>rank<td>", cnt,
                          "<tr><td>cdate<td>",
                          "UTC:", tmp.strftime("%m/%d %H:%M"),"<br>",
                          "美東:", dt.to_us(tmp).strftime("%m/%d %H:%M"),"<br>",
                          "盤古:", dt.to_taipei(tmp).strftime("%m/%d %H:%M"),
                          "<tr><td>repost<td>", i['repost'],
                          "<tr><td>max_views<td>", i['max_views'],
                          "<tr><td>like<td>", i['like'],
                          "<tr><td>comment<td>", i['comment'],
                          "<tr><td>lang<td>", i['prev_lang'],
                          "<tr><td>video<td>", videoTxt,
                          "</table><td>", file=f)
                    if i['previmg'].find("http") == 0:
                        print("<img alt='[No Image]' loading='lazy' src='"+i['previmg']+"'/>", file=f)
                    print("</table>", file=f)
                    print("<p>", i['txt'], file=f)
                print("<br><hr><br><a href='/'>Home</a> ", file=f)
                for p in range(totalPage):
                    lb = '[' if p==page else ' '
                    rb = ']' if p==page else ' '
                    print("<a href='"+linkHtml(colName, url=True, lang=lang, sortKey=sortKey, page=p)+"'>",
                            lb+str(p)+rb,"</a> ", file=f)
                print("</body></html>", file=f)
                f.close()

    def summaryLinks(self, colName, lang):
        self.D("Process",colName)
        col = self.db[colName]
        links = {}
        for item in col.find({'prevsrc':{'$exists':True}}):
            k = item['prevsrc']
            if lang !="" and item['prev_lang'] != lang:
                continue
            if item['prevsrc'].lower().find("gnews.org")>0: # skip gnews.org link
                continue
            if k not in links:
                links[k] = {}
                dbLink = self.linkDB.find(k)  # load from database
                if not dbLink:
                    self.D("Find new link", k, minGap=10, timeIt=True)
                else:
                    links[k]['cdate'] = dt.dt(dbLink['cdate'])
                    self.D("Exist link", k, minGap=10, timeIt=True)
                self.setStr(links[k], 'prevsrc', item, 'prevsrc')
                self.setStr(links[k], 'previmg', item, 'previmg')
                self.setStr(links[k], 'ttl', item, 'ttl')
                self.setStr(links[k], 'dsc', item, 'dsc')
                self.setStr(links[k], 'prev_lang', item, 'prev_lang')
                self.setStr(links[k], 'pid', item, 'pid', 'https://www.gettr.com/post/')
                if 'cdate' not in links[k]: # assign cdate only first appear
                    links[k]['cdate'] = dt.dt(item['cdate'])
                links[k]['post_cnt'] = 0  # reset to count 24 hours

            if links[k]['cdate'] > dt.dt(item['cdate']): # keep oldest cdate
                links[k]['cdate'] = dt.dt(item['cdate'])
                self.setStr(links[k], 'pid', item, 'pid', 'https://www.gettr.com/post/')
            links[k]['post_cnt'] +=1

            self.setStr(links[k], 'vid', item, 'vid')
            self.maxInt(links[k], "vid_dur", item, "vid_dur")
            self.maxInt(links[k], "vid_wid", item, "vid_wid")
            self.maxInt(links[k], "vid_hgt", item, "vid_hgt")

            self.addInt(links[k], 'like', item, 'lkbpst')
            self.addInt(links[k], 'comment', item, 'cm')
            self.addInt(links[k], 'repost', item, 'shbpst')
            self.addInt(links[k], 'max_views', item, 'max_views')

            self.appendFuzzyStr(links[k], 'txt', item, 'txt',
                "<b>"+item['uid']+'(<a href="https://www.gettr.com/post/'+item['pid']+'">'+str(item['max_views'])+'</a>)</b>')
            if not self.appendFuzzyStr(links[k], 'txt', item, 'txt_zhs', '<b>[翻譯]</b>'):
                self.appendFuzzyStr(links[k], 'txt', item, 'txt_zht', '<b>[翻譯]</b>')

        # update to database
        for k in links:
            self.D("Update link DB", k, minGap=10, timeIt=True)
            self.linkDB.add(links[k])

        # filter out > 1 days links
        newLinks = {}
        for k in links:
            if dt.dt(links[k]['cdate']) >= self.acceptMinDate:
                newLinks[k] = links[k]
            else:
                self.D("[drop link]", k, links[k]["cdate"], "<", self.acceptMinDate, minGap = 10, timeIt = True)
        links = newLinks


        tables = {}
        for k in links:
            for f in links[k]:
                if f not in tables:
                    tables[f] = []
                tables[f].append(links[k][f])

        df = pandas.DataFrame(tables, columns = [
                'rank','like','comment','repost','max_views','post_cnt','ttl','dsc','txt','cdate','prevsrc','previmg','pid','prev_lang'])
        df = df.sort_values('repost', ascending = False)
        # add rank column
        for idx in range(len(df)):
            df.iat[idx, 0] = idx+1

        self.D("Export", linkCsv(colName, lang=lang))
        df.to_csv(linkCsv(colName, lang=lang), index = False, encoding = 'utf-8')

        cmd = '/usr/bin/gzip -f '+linkCsv(colName, lang=lang)
        self.D("Execute",cmd)
        os.system(cmd)

        for sortKey in ["repost", "max_views","cdate"]:
            self.generateHTML(colName, links, lang, sortKey=sortKey)

    def run(self, noSkip = False, processOne = False):
        self.D("noSkip", noSkip)
        self.D("processOne", processOne)

        self.acceptMinDate = dt.now_delta(days = -1)
        self.D("accepted min date:", self.acceptMinDate)

        self.connection = dbConnection.dbConnection(connect = True, logLevel = ut.DEBUG_LEVEL)
        self.D(self.connection)
        self.db = self.connection['report']
        self.D(self.db)
        self.linkDB = gettrLinkDB.gettrLinkDB(self.connection)

        cols = self.db.list_collection_names()
        cols.sort(reverse=True)
        lang = ["", "zh"]
        self.removeOutOfDate(cols, lang)
        for colName in cols:
            for l in lang:
                if noSkip or not os.path.exists(linkCsv(colName, lang=l, gzip=True)):
                    self.summaryLinks(colName, l)
                else:
                    self.D("Skip", colName,"due to ", colName, "exists.")

            if processOne:
                break

        self.linkDB = None
        self.db = None
        self.connection = None

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Export links statistics html')
    parser.add_argument('--no-skip', action='store_true', default=False,
                        help='reprocess all database')
    parser.add_argument('--one', action='store_true', default=False,
                        help='process latest one only')
    args = parser.parse_args()
    t = exportLinks()
    t.run(noSkip = args.no_skip, processOne = args.one)
