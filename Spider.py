from dataclasses import dataclass, field
from typing import Any
from scrapy.http import FormRequest
import DTime
import heapq
import Lid
import Log
import os
import queue
import scrapy
import threading
import traceback

gQueueSizeLock = threading.Lock()
gQueueSize = {}
gQueueSizeSum = 0

@dataclass(order=True)
class TQueueItem:
    timeout: int
    item: Any=field(compare=False)

class Spider:
    def cleanStuff(self):
        self.spiderQueen = None
        self.dbPoolLock = None
        self.dbPool = None
        self.spiderPoolLock = None
        self.spiderPool = None
        self.lid = None

    def __init__(self, name, commander=False):
        self.log = Log.Log(who=name)
        #self.log.setLogLevel(Log.VERBOSE_LEVEL)
        self.name = name
        self.commander = commander
        self.log.V("constructor")

        self.deadEvent = threading.Event()
        self.queue = queue.Queue()
        self.lazyQueueLock = threading.Lock()
        self.lazyQueue = queue.Queue()

        self.timeQueueLock = threading.Lock()
        self.timeQueue = []
        self.lastTimeActive = DTime.now()

        self.cleanStuff()

        threading.Thread(target= self.queueLoop, name=self.name).start()

    def __del__(self):
        #assert self.queue.qsize() == 0
        if self.log:
            self.log.D("Spider __del__", self.name)

    def getGlobalQueueSize(self):
        global gQueueSizeLock
        global gQueueSizeSum
        with gQueueSizeLock:
            return gQueueSizeSum

    def incGlobalQueueSize(self, target):
        global gQueueSizeLock
        global gQueueSize
        global gQueueSizeSum
        with gQueueSizeLock:
            if target not in gQueueSize:
                gQueueSize[target] = 0
            gQueueSize[target] += 1
            gQueueSizeSum += 1

    def decGlobalQueueSize(self, target):
        global gQueueSizeLock
        global gQueueSize
        global gQueueSizeSum
        with gQueueSizeLock:
            if target not in gQueueSize:
                gQueueSize[target] = 0
            gQueueSize[target] -=1
            gQueueSizeSum -= 1

    def showGlobalQueueSize(self):
        global gQueueSizeLock
        global gQueueSize
        global gQueueSizeSum
        with gQueueSizeLock:
            X = []
            for (target, n) in gQueueSize.items():
                X.append(f"[{target}:{n}]")
            self.log.D("GlobalQueue", ' '.join(X), minGap=17)

    def stop(self):
        self.log.D("stop()")
        if not self.deadEvent.is_set():
            self.log.D("send exit msg")
            self.queue.put(None)
            self.incGlobalQueueSize(self.name)
            self.deadEvent.wait()
            self.log.D("stopped")
        else:
            self.log.W("alread stopped!!")

        self.cleanStuff()

    def enque(self, target, X, timeout=0):
        if target =='queen':
            if self.spiderQueen:
                spider = self.spiderQueen
            else:
                spider = self
        else:
            with self.spiderPoolLock:
                if target in self.spiderPool:
                    spider = self.spiderPool[target]
                else:
                    self.log.W("enque fail due to spider not exists!", target)
                    return

        assert X != None
        self.log.V(self.name , "enque to ", spider.name, X)
        if timeout == 0:
            spider.queue.put(X)
            spider.incGlobalQueueSize(target)
        else:
            spider.putTimeQueue(timeout, (target, X))

    def dequeLazyJob(self):
        cnt = 0
        with self.lazyQueueLock:
            while self.lazyQueue.qsize()>0:
                X = self.lazyQueue.get()
                self.log.D("[Lazy Job] deque", X)
                self.enque(X[0], X[1])
                cnt += 1
        return cnt > 0

    def enqueLazyJob(self, target, X):
        with self.lazyQueueLock:
            self.log.D("[Lazy Job] enque", target, X)
            self.lazyQueue.put((target, X))

    def putTimeQueue(self, timeout, X):
        with self.timeQueueLock:
            heapq.heappush(self.timeQueue, TQueueItem(timeout,X))
            #self.log.D("[TimeQueue] put", X)

    def getTimeQueue(self):
        if self.isExit():
            #self.log.D("[TimeQueue] abort due to isExit", minGap=7)
            return None

        with self.timeQueueLock:
            if len(self.timeQueue) > 0:
                dur = (DTime.now() - self.lastTimeActive).seconds
                if self.timeQueue[0].timeout <= dur:
                    X = heapq.heappop(self.timeQueue)
                    #self.log.D("[TimeQueue] get", X, timeout)
                    return X
        return None

    def setLastTimeActive(self, time):
        with self.timeQueueLock:
            self.lastTimeActive = max(self.lastTimeActive, time)
            #self.log.D("[TimeQueue] settime", self.lastTimeActive, time, minGap=7)

    def queueLoop(self):
        try:
            while True:
                try:
                    X = self.queue.get(timeout=1)
                    self.decGlobalQueueSize(self.name)
                    if X:
                        #self.log.W("handle", X)
                        self.handle(X)
                    else:
                        self.log.D("Receive EXIT msg")
                        self.handleClose()
                        self.deadEvent.set()
                        break
                except queue.Empty:
                    X = self.getTimeQueue()
                    if X:
                        target = X.item[0]
                        Y = X.item[1]
                        self.log.D("[TimeQueue] enque", target, Y, minGap=7*7)
                        self.enque(target, Y)
                    #else:
                    #    if self.commander:
                    #        self.log.W("send unregister to queen")
                    #        self.enque('queen', ('unregister', self.name))
        except:
            traceback.print_exc()
            os._exit(1)

    def register(self, cycleHours=1, spider=None):
        # cycleHours means how hours per cycle (20 means 20 hours need loop 1 cycle)
        assert self.spiderQueen == None # no queen yet
        if spider == None: # I'm Queen
            self.log.V("create singleton queen stuff for all kids")
            self.spiderQueen = None
            self.eventExit = threading.Event()
            self.dbPoolLock = threading.Lock()
            self.dbPool = {}
            self.spiderPoolLock = threading.Lock()
            self.spiderPool = {}
            self.lid = Lid.Lid()
            self.cycleHours = {}
            self.priority = {}
            self.maxPriority = 0
            self.maxCycleHours = 0
            self.curCycleHours = 0
            self.start()
        else: # kids
            self.maxCycleHours = max(cycleHours, self.maxCycleHours)
            self.cycleHours[spider.name] = cycleHours
            self.priority[spider.name] = 0
            spider.log.V(spider.name, "get stuff from", self.name)
            spider.spiderQueen = self
            spider.eventExit = self.eventExit
            spider.dbPoolLock = self.dbPoolLock
            spider.dbPool = self.dbPool
            spider.spiderPoolLock = self.spiderPoolLock
            spider.spiderPool = self.spiderPool
            spider.lid = self.lid

            spider.log.V(spider.name, "register to", self.name)
            with self.spiderPoolLock:
                name = spider.name
                self.spiderPool[name] = spider
            spider.start()

    def unregister(self, name):
        spider = self.getSpider(name)
        if not spider:
            self.log.W("unregister failed! unknown", name)
            return

        self.log.V("call", name, "spider to close", spider)
        spider.stop()
        self.delSpider(name)

    def isExit(self):
        return self.eventExit.isSet()

    def getDB(self, tableName):
        self.log.E("No related to database")

    def delSpider(self, name):
        spider = None
        with self.spiderPoolLock:
            if name in self.spiderPool:
                spider = self.spiderPool[name]
                del self.spiderPool[name]
        return spider

    def getSpider(self, name):
        with self.spiderPoolLock:
            if name in self.spiderPool:
                return self.spiderPool[name]
            else:
                return None

    def getSpiderCount(self):
        with self.spiderPoolLock:
            return len(self.spiderPool)

    def getSpiderCommanderList(self):
        with self.spiderPoolLock:
            lst = []
            for (name, spider) in self.spiderPool.items():
                if spider.commander:
                    lst.append(name)
            return lst

    def stopAllSpiders(self):
        try:
            while True:
                spider = None
                with self.spiderPoolLock:
                    name = next(iter(self.spiderPool))
                    spider = self.spiderPool[name]

                self.log.D("stopAllSpiders() call", name, "spider to close", spider)
                spider.stop()

                with self.spiderPoolLock:
                    del self.spiderPool[name]
        except:
            pass
        self.log.D("stopAllSpiders() done")

    def start(self):
        self.log.W("start() not implement!")

    def handle(self, X):
        self.log.W("handle() not implement!")

    def handleClose(self):
        self.log.W("handleClose() not implement!")

    def pollRequest(self):
        self.log.W("pollRequest() not implement!")

    def extractStr(self, item, ik, d, dk):
        if dk in d:
            if d[dk] == None:
                item[ik] = ""
            else:
                item[ik] = str(d[dk]).strip()
    def extractInt(self, item, ik, d, dk):
        if dk in d:
            item[ik] = int(float(d[dk]))
    def extractDatetime(self, item, ik, d, dk):
        if dk in d:
            item[ik] = DTime.fromtimestamp(float(d[dk])/1000.0)

    def sendKingRequest(self, X, target,  meta={}, headers={}, cookies={}, timeout=0):
        meta = meta.copy()
        meta["target"] = target
        self.enque('king', ('request', X, meta), timeout)

    def sendQueenRequest(self, X, target,  meta={}, headers={}, cookies={}, timeout=0):
        meta = meta.copy()
        meta["target"] = target
        self.enque('queen', ('request', X, meta, headers, cookies), timeout)

    def sendQueenFormRequest(self, response, target, formdata={},  meta={}, headers={}, cookies={}, timeout=0):
        X = FormRequest.from_response(response, formdata=formdata)
        self.sendQueenRequest(X, target, meta, headers, cookies, timeout)

    def sendQueenUnregister(self):
        self.enque('queen', ('unregister', self.name))

    def sendQueryTranslate(self, pid, fieldName, url, target, meta={}, headers={}, cookies={}):
        meta = meta.copy()
        meta["pid"] = pid
        meta["fieldName"] = fieldName
        self.sendQueenRequest(url, target, meta, headers, cookies)

    def sendResponse(self, response):
        if 'target' in response.meta:
            name = response.meta['target']
            self.enque(name, ('response', response, response.meta))
        else:
            self.log.W("unknown response target", response)

    def sendAccountQuery(self, uid):
        self.enque('accou', ('query', uid))

    def sendAccountFollowData(self, data):
        self.enque('accou', ('handleFollowData', data))

def main():
    spider = Spider("test")

    for x in range(10):
        spider.enque(('handle',x))
    spider.stop()

if __name__ == '__main__':
    main()


