import datetime
import pytz
import unittest

def DTime(d):
    return d.replace(tzinfo=pytz.utc)

def ymdhm(y,m,d,H,M):
    return DTime(datetime.datetime(y,m,d,H,M,tzinfo=pytz.utc))

def now():
    d = datetime.datetime.utcnow()
    return d.replace(tzinfo=pytz.utc)

def add_delta(d, days = 0, hours = 0, minutes = 0, seconds = 0):
    return DTime(d) + datetime.timedelta(days = days, hours = hours, minutes = minutes, seconds = seconds)

def now_delta(days = 0, hours = 0, minutes = 0, seconds = 0):
    return add_delta(now(), days = days, hours = hours, minutes = minutes, seconds = seconds)

def fromtimestamp(timestamp):
    tmp = datetime.datetime.fromtimestamp(float(timestamp), pytz.utc)
    return tmp.replace(tzinfo=pytz.utc)

def fromtimestamp1000(timestamp1000):
    return fromtimestamp(float(timestamp1000)/1000.0)

def type():
    return datetime.datetime

def same_type(d):
    return isinstance(d, datetime.datetime)

def to_taipei(d):
    return DTime(d).astimezone(pytz.timezone('Asia/Taipei'))

def to_us(d):
    return DTime(d).astimezone(pytz.timezone('US/Eastern'))

def to_utc(d):
    return d.astimezone(pytz.UTC)


class UnitTest(unittest.TestCase):
    def test_now(self):
        d = now()
        self.assertEqual(d.tzinfo, pytz.UTC)

    def test_fromtimestamp(self):
        d = now()
        dt = d.timestamp()
        self.assertEqual(fromtimestamp1000(str(dt*1000)), d)

    def test_DTime(self):
        d = datetime.datetime.utcnow()
        self.assertEqual(d.tzinfo, None)
        d = DTime(d)
        self.assertEqual(d.tzinfo, pytz.UTC)

    def test_delta(self):
        d = datetime.datetime.utcnow()
        x = add_delta(add_delta(d, days=1), hours=-24)
        self.assertEqual(x.tzinfo, pytz.UTC)
        self.assertEqual(DTime(d), x)

        d = now()
        x = add_delta(add_delta(d, days=1), hours=-24)
        self.assertEqual(x.tzinfo, pytz.UTC)
        self.assertEqual(DTime(d), x)

    def test_type(self):
        d = datetime.datetime.now()
        self.assertTrue(isinstance(d, datetime.datetime))
        self.assertTrue(isinstance(d, type()))
        self.assertTrue(same_type(d))

    def test_timezone(self):
        ud = datetime.datetime.utcnow()
        tpe = to_taipei(ud)
        x = to_utc(tpe)
        us = to_us(x)
        x = to_utc(us)
        self.assertEqual(DTime(ud), x)

    def test_ymdhm(self):
        d = ymdhm(1977,1,17,12,34)
        self.assertEqual(d.tzinfo, pytz.UTC)
        self.assertEqual(d.year, 1977)
        self.assertEqual(d.month, 1)
        self.assertEqual(d.day, 17)
        self.assertEqual(d.hour, 12)
        self.assertEqual(d.minute, 34)

if __name__ == "__main__":
    unittest.main()
