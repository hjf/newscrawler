import DictDB

class gettrLinkDB(DictDB.DictDB):
    def __init__(self, dbConnection, dbName = "gettr", dbTable = "links"):
        super().__init__(dbConnection, dbName = dbName, dbTable = dbTable, keyField = "prevsrc",
                         ioCache = 777, maxCache = 77777,
                         logLevel = ut.DEBUG_LEVEL)
        self.create_index("prevsrc", unique = True)
        self.create_index([("ctime",1)])

if __name__ == '__main__':
    pass
