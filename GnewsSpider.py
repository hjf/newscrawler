import GnewsDBAccount
import GnewsDBPost
import Spider

class GnewsSpider(Spider.Spider):
    def getDB(self, tableName):
        with self.dbPoolLock:
            if tableName not in self.dbPool:
                if tableName == 'account':
                    self.dbPool[tableName] = GnewsDBAccount.GnewsDBAccount(drop=False)
                elif tableName == 'post':
                    self.dbPool[tableName] = GnewsDBPost.GnewsDBPost(drop=False)
                else:
                    self.log.W("unknown DB", tableName)
            return self.dbPool[tableName]

    def sendAccountData(self, data):
        self.enque('accou', ('handleData', data))


    def sendPostData(self, data):
        self.enque('post', ('handleData', data))

    def sendPostQuery(self, pid):
        self.enque('post', ('query', pid))
