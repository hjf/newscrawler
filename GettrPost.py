import GettrSpider
import scrapy
import DTime
import json
import Log

MIN_POST_NUM=77

class GettrPost(GettrSpider.GettrSpider):
    def start(self):
        self.log.setTextColor(Log.COLOR256_208)
        self.enque('post', ('scanAccountPost', DTime.now()))

    def handle(self, X):
        if X[0] == 'response':
            self.handlePost(X[1])
            return

        if X[0] == 'scanAccountPost':
            self.scanAccountPost(maxTime = X[1])
            return

        self.log.W("unknown", X)

    def handleClose(self):
        pass

    def scanAccountPost(self, maxTime):
        if maxTime == None:
            maxTime = DTime.now()
        accountDB = self.getDB('account')
        X = accountDB.query("""select * from account
            where gettr_follower >= 500 and (crawler_post_time < %s or crawler_post_time is null)
                  and status != 'deleted'
            order by crawler_post_time limit 100000""", (maxTime,))
        self.log.D("scanAccountPost maxTime", maxTime)
        updateCnt = 0
        if X:
            totalCount = accountDB.count()
            cnt=0
            for uid in X:
                cnt += 1
                offset = 0
                item = accountDB.get(uid)
                minTime = item['crawler_post_time']
                if minTime == None:
                    minTime = DTime.now_delta(days=-1)

                if minTime < maxTime and item["status"] != "deleted":
                    self.log.D("scanAccountPost:", cnt, '/', len(X), totalCount, "uid:", uid, "minTime:", minTime, "maxTime:", maxTime, timeIt=True, minGap=7)
                    updateCnt += 1
                    self.scanAccountPostPage(uid, 0, minTime, maxTime)
        if updateCnt>0 and len(X) >= MIN_POST_NUM:
            self.enque('post', ('scanAccountPost', maxTime), timeout=77)
        else:
            postDB = self.getDB('post')
            postDB.flush()
            #self.spiderQueen.enqueLazyJob('post', ('scanAccountPost', None))
            self.enque('post', ('scanAccountPost', None), timeout=77)

    def scanAccountPostPage(self, uid, offset, minTime, maxTime):
        url = f"https://api.gettr.com/u/user/{uid}/posts?offset={offset}&max=20&dir=fwd&incl=posts|stats|userinfo|shared|liked&fp=f_uo"
        meta={'queryUid':uid, 'offset':offset, 'minTime':minTime, 'maxTime':maxTime, 'handle_httpstatus_all': True}
        self.sendQueenRequest(url, 'post', meta=meta)


    def handlePost(self, X):
        accountDB = self.getDB('account')
        postDB = self.getDB('post')

        meta = X.meta

        queryUid = meta['queryUid']
        offset = meta['offset']
        minTime = meta['minTime']
        maxTime = meta['maxTime']

        queryUser = accountDB.get(queryUid)

        if X.status == 400:
            queryUser["status"] = "deleted"
            self.log.W("deleted user", queryUser)
            accountDB.set(queryUser)
            return

        if X.status == 520:
            self.log.W("unknown error", X.url)
            return

        if X.text == None:
            self.log.W(".text is None", X.url)
            return

        queryUser['crawler_post_time'] = maxTime
        accountDB.set(queryUser)

        j = json.loads(X.text)
        # query repost cdate
        repostCdate = {}
        try:
            d = j["result"]["data"]["list"]
            for lst in d:
                if "activity" in lst:
                    pid = lst["activity"]["pstid"]
                    self.extractDatetime(repostCdate, pid, lst, "cdate")
        except:
            pass

        # parsing User data
        try:
            userdata = j["result"]["aux"]["uinf"]
            self.sendAccountFollowData(X)
        except:
            pass

        # parsing Post
        try:
            d = j["result"]["aux"]["post"]
        except:
            return

        isSkipped = False
        isUpdated = False
        for pid in d:
            post = d[pid]
            item = {}
            item["pid"] = pid
            self.extractDatetime(item, "cdate", post, "cdate")
            if item["cdate"] > DTime.now():
                self.log.W("cdate > current time", pid, item["cdate"])
                item["cdate"] = DTime.now()

            if pid in repostCdate:
                item["udate"] = repostCdate[pid]
            else:
                item["udate"] = item["cdate"]

            if item["udate"] < item["cdate"]:
                self.log.W("udate < cdate", pid, item["udate"], item["cdate"], repostCdate[pid])
                item["udate"] = item["cdate"]

            if item["udate"] > DTime.now():
                self.log.W("udate > current time", pid, item["udate"])
                item["udate"] = DTime.now()

            self.extractStr(item, "uid", post, "uid")

            #user = accountDB.get(item["uid"])
            #if not user:
            #    self.log.D("[New user]", item["uid"], minGap = 7, timeIt = True)
            #    self.sendAccountQuery(item["uid"])

            if item['udate'] <= minTime: # or maxTime < item['udate']:
                isSkipped = True
                #self.log.W("skip post", queryUid, pid, minTime, item, minGap=3)
                continue

            if item['udate'] > queryUser['crawler_post_time']:
                queryUser['crawler_post_time'] = item['udate']
                accountDB.set(queryUser)

            self.extractStr(item, "txt", post, "txt")
            self.extractStr(item, "txt_lang", post, "txt_lang")
            self.extractStr(item, "prevsrc", post, "prevsrc")
            self.extractStr(item, "previmg", post, "previmg")
            self.extractStr(item, "ttl", post, "ttl")
            self.extractStr(item, "dsc", post, "dsc")
            self.extractStr(item, "vid", post, "vid")
            self.extractInt(item, "vid_dur", post, "vid_dur")
            self.extractInt(item, "vid_wid", post, "vid_wid")
            self.extractInt(item, "vid_hgt", post, "vid_hgt")
            if "prevsrc" in item:
                txt = ""
                if "ttl" in item:
                    txt += item["ttl"]
                if "dsc" in item:
                    txt += item["dsc"]
                txt = txt.replace('\n','')
                item["prev_lang"] = self.lid.getLang(txt)

                if 'vid' not in item:
                    if item['prevsrc'].find('gtv.org/video/')>0:
                        item['vid']="[GTV]"

            self.extractInt(item, "lkbpst", post, "lkbpst")
            self.extractInt(item, "cm", post, "cm")
            self.extractInt(item, "shbpst", post, "shbpst")

            if "htgs" in post:
                item["htgs"] = '#'.join(post["htgs"])
            if "utgs" in post:
                item["utgs"] = '@'.join(post["utgs"])

            item['max_views'] = queryUser['gettr_follower']
            # append the max_views from database
            oldPost = postDB.get(pid)
            if oldPost:
                item['max_views'] += oldPost['max_views']
                isNew = False
            else:
                isNew = True

            if "translates" in post:
                if (not oldPost) or ("txt_en" not in oldPost):
                    if "en" in post["translates"]:
                        if "translatedPath" in post["translates"]["en"]:
                            self.sendQueryTranslate(pid, "txt_en", post["translates"]["en"]["translatedPath"], "trans")

                if (not oldPost) or ("txt_zhs" not in oldPost):
                    if "zh-Hans" in post["translates"]:
                        if "translatedPath" in post["translates"]["zh-Hans"]:
                            self.sendQueryTranslate(pid, "txt_zhs", post["translates"]["zh-Hans"]["translatedPath"], "trans")

                if (not oldPost) or ("txt_zht" not in oldPost):
                    if "zh-Hant" in post["translates"]:
                        if "translatedPath" in post["translates"]["zh-Hant"]:
                            self.sendQueryTranslate(pid, "txt_zht", post["translates"]["zh-Hant"]["translatedPath"], "trans")

            if postDB.set(item):
                if isNew:
                    self.log.D("new post", pid, item["cdate"], item["uid"], timeIt=True, minGap=3)
                else:
                    self.log.D("update post", pid, timeIt=True, minGap=3)
                isUpdated = True


        if isUpdated and not isSkipped:
            offset +=20
            self.log.D("next page", queryUid, offset, minTime, maxTime, minGap=7)
            self.scanAccountPostPage(queryUid, offset, minTime, maxTime)


if __name__ == '__main__':
    trend = SpiderPost('post', "[Post]")
    trend.stop()
    trend = None

