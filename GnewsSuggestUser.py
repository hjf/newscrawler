import GnewsSpider
import scrapy
import json
import traceback

class GnewsSuggestUser(GnewsSpider.GnewsSpider):
    def start(self):
        url = "https://api.gnews.org/u/feed/suggestusers?max=20"
        self.sendQueenRequest(url, 'suggu')

    def handle(self, X):
        if X[0] == 'response':
            j = json.loads(X[1].text)
            try:
                d = j["result"]["aux"]["uinf"]
            except:
                traceback.print_exc()
                return

            self.sendAccountData(X[1])
            self.sendQueenUnregister()

    def handleClose(self):
        pass

