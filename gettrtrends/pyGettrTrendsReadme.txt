GETTR Trends Readme

需要python3.8+

安装的包 
pip install pandas
pip install tornado
pip install wget
pip install numpy
pip install seaborn
pip install matplotlib


需要的文件
pyGettrTrendsindex.html 用于设置关键词的首页
pygettranaserv.py       gettrana服务器主脚本
pygettrana.py           数据分析的函数
pyGettrTrendsReadme.txt 说明文本
static/favicon.ico      网站ico图标

运行方式：
python pygettranaserv.py --date=now                 {载入当前时间的最新数据库}
python pygettranaserv.py --date=20220114            {载入指定日期的数据库 时间固定为 UTC 0017}
python pygettranaserv.py --date=20220114-20220117   {载入指定日期区间的数据库 时间固定为 UTC 0017}

python pygettranaserv.py --file=folder_path/post20220101_0017F500D1.csv.gz   {载入指定路径的csv.gz文件}

运行过程会检查down文件夹是否存在需要的文件，根据需要自动下载相应文件。根据服务器性能需花一些时间将数据库载入内存，之后会打开8000端口的监听。

在浏览器中打开 http://127.0.0.1:8000 即有界面了。可将该端口直接暴露于公网或使用nginx转发，同时开启TLS提高安全性。



更新记录：
20220118 轻度重构代码，整理了pymytornado pygettrana中包含的函数，优化函数名称。加入了载入一段日期区间数据库、直接指定数据库文件的功能。增加了static/favicon.ico。
20220117 html中加入了快速选择关键字的功能，加入了账号黑名单。更新了在服务器终端的日志反馈格式。
20220115 初步完成了tornado服务器，可架设网页服务器。
20220109 在jupyter中完成了初步的推荐算法，绘制相关图表。输出结果可整理为mediawiki及gettr帖子可以使用的文本格式。