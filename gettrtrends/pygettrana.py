import pandas as pd, numpy as np, seaborn as sns, matplotlib, matplotlib.pyplot as plt
import gzip, datetime, os, dateutil, math, wget, datetime
from datetime import timezone


# pd.options.display.float_format = '{:.3f}'.format
# np.set_printoptions(precision=3, suppress=True)

# now = datetime.datetime.now()
# year = '{:04d}'.format(now.year)
# month = '{:02d}'.format(now.month)
# day = '{:02d}'.format(now.day)
# hour = '{:02d}'.format(now.hour)
# minute = '{:02d}'.format(now.minute)

# 移除df的index
def remove_df_index(df,string=''):
    blankIndex=[string] * len(df)
    df.index=blankIndex
    return df

# 获取时间戳
def nowtime():
    nowdatetime=datetime.datetime.utcnow().isoformat()
    return "["+nowdatetime+"]"

# 整理变量格式
def dftoint(df):
    df['点赞数']=df['点赞数'].astype("int")
    df['留言数']=df['留言数'].astype("int")
    df['转贴数']=df['转贴数'].astype("int")
    df['粉丝数']=df['粉丝数'].astype("int")
    df['阅读次数']=df['阅读次数'].astype("int")
    df['视频时长']=df['视频时长'].astype("int")
    return df

# 生成日期列表
def create_date_list(beginDate, endDate):
    if int(beginDate)>=int(endDate):
        date_l=[datetime.datetime.strftime(x,'%Y%m%d') for x in list(pd.date_range(start=beginDate, end=beginDate))]
    else:
        date_l=[datetime.datetime.strftime(x,'%Y%m%d') for x in list(pd.date_range(start=beginDate, end=endDate))]
    return date_l

# 生成文件名
def create_savefilename(startdate,enddate,sufix=''):
    savefilename=''
    if startdate>=enddate:
        savefilename=startdate+sufix
    else:
        savefilename=startdate+'to'+enddate+sufix
    return savefilename

# 生成文件名清单
def create_csv_filelist(datelist,downhour):
    dfdate=pd.DataFrame(datelist)
    dfdate['filelist']="post"+dfdate[0]+downhour+"F500D1.csv.gz"
    filelist=dfdate['filelist'].tolist()
    return filelist

# 从文件读取单个文件
def get_df_fromfile(filename,downpath='down/'):
    with gzip.open(downpath+filename) as f:
        df = pd.read_csv(f)
    return df

# 根据文件列表建立DataFrame
def get_df_fromlist(filelist,downpath='down/',cols_to_drop=['udate','prevsrc','previmg','prev_lang','htgs','utgs','max_viewers','vid','vid_wid','vid_hgt','ttl','dsc','txt_zhs','txt_zht','txt_en']):
    df=pd.DataFrame()
    print(nowtime(),'Now loading the files...')
    for filename in filelist:
        down_csv(filename,downpath)
        if df.empty:
            df=get_df_fromfile(filename=filename,downpath=downpath)
        else:
            df=pd.concat([df,get_df_fromfile(filename)],ignore_index=True)
    df=df_init(df)
    return df

# 做出初步整理
def df_init(df,cols_to_drop):
    df.rename(columns={"cdate": "发帖时间",
                       "max_views": "阅读次数",
                       "lkbpst": "点赞数",
                       "shbpst": "转贴数",
                       "cm": "留言数",
                       "vid_dur":"视频时长",
                       "gettr_follower":"粉丝数",
                       "txt_lang":"语言"},inplace=True)
    df['发帖时间']=pd.to_datetime(df['发帖时间'])
    df=df.drop(cols_to_drop,axis=1)
    print(nowtime(),"df Loaded to Memory")
    return df

# 下载、检查数据库文件
def down_csv(filename,downpath):
    makedir(downpath)
    filepath=downpath+filename
    
    if not os.path.exists(filepath):
        print(nowtime(),'{} not found, try to download'.format(filepath))
        
        url = "http://mosnews.orkmail.com/export/"+filename
        wget.download(url,filepath)
        print("\n"+nowtime(),'Downloaded:',filepath)
    else:
        print(nowtime(),'File Found:',filepath)

# 计算标化率
def normalize_cols(df,cols_to_normalize=['观看率','点赞率','转帖率','留言率'],usemaxcsv=False):
    if usemaxcsv==True:
        dfmax=pd.read_csv('maxvalue.csv',index_col=0,header=None)
        max_dict=dfmax.to_dict(orient='dict')[1]
        for col in cols_to_normalize:
            df['标化'+col] = df[col]/max_dict[col]
    else:
        for col in cols_to_normalize:
            df['标化'+col] = (df[col]-df[col].min())/(df[col].max()-df[col].min())
    return df

# 更新最大值
def update_maxiumcsv(df):
    dfmax=pd.read_csv('maxvalue.csv',index_col=0,header=None)
    dfmax[1]=[df['阅读次数'].max(),df['点赞数'].max(),df['转贴数'].max(),df['留言数'].max()]
    dfmax.to_csv('maxvalue.csv',index=True,header=False)

# df筛选语言
def get_df_by_lan(df,text_langs=['zh','tw'],by='',**kargs):
    if text_langs!='':
        df=df[df["语言"].isin(text_langs)]
    if by!='':
        df=dealdf(df,by=by,**kargs)
    return df.copy()

# 过滤语言
def flt_lang(df,langs=['zh','tw']):
    langs= '|'.join(langs)
    df=df[df['语言'].str.contains(langs,na=False,case=False)].copy()
    return df

# 搜索文本
def flt_txt(df,keywords=[''],keysep=True,stopwords=['-'],stopsep=True,by='score',**kargs):
    if keywords!=['-']:
        if keysep==True:
            keywords= '|'.join(keywords)
            df=df[df['txt'].str.contains(keywords,na=False,case=False)].copy()
        else:
            for key in keywords:
                df=df[df['txt'].str.contains(key,na=False,case=False)].copy()
                df['txt'].replace(key, '<b style="color:blue">'+key+'</b>', inplace=True, regex=True)
                
    if keywords!=['-']:
        if stopsep==True:
            stopwords= '|'.join(stopwords)
            df=df[df['txt'].str.contains(stopwords,na=False,case=False)== False].copy()
        else:
            for key in stopwords:
                df=df[df['txt'].str.contains(key,na=False,case=False)== False].copy()
    return df

# 过滤账号
def flt_uid(df,blackuidlist=""):
    if blackuidlist!="":
        blackuidlist= '|'.join(blackuidlist)
        df=df[df['uid'].str.match(blackuidlist,na=False,case=False)== False].copy()
    return df

# df 按文本筛选
def filter_df_txt(df,keywords=[''],keysep=True,stopwords=['-'],stopsep=True,by='score',**kargs):
    if keywords!=['-']:
        if keysep==True:
            keywords= '|'.join(keywords)
            df=df[df['txt'].str.contains(keywords,na=False,case=False)].copy()
        else:
            for key in keywords:
                df=df[df['txt'].str.contains(key,na=False,case=False)].copy()
    if keywords!=['-']:
        if stopsep==True:
            stopwords= '|'.join(stopwords)
            df=df[df['txt'].str.contains(stopwords,na=False,case=False)== False].copy()
        else:
            for key in stopwords:
                df=df[df['txt'].str.contains(key,na=False,case=False)== False].copy()
    return df

# makedir
def makedir(path):
    if not os.path.exists(path):
        print('Creating Folder:'+path)
        os.makedirs(path)

# 计算各种参数
def dealdf(df,by='score',min_follower=777,**kargs):
    df=df[df['粉丝数']>min_follower].copy()
    
    df['观看率']=df['阅读次数']/df['粉丝数']
    df['点赞率']=df['点赞数']/df['粉丝数']
    df['转帖率']=df['转贴数']/df['粉丝数']
    df['留言率']=df['留言数']/df['粉丝数']
    
    df=df.fillna(0)
    
    df=dftoint(df)
    
    df=normalize_cols(df)
    df=calc_score(df)
    
    df=normalize_cols(df,cols_to_normalize=['score'])
    
    df['txt'].replace('\n',' ',inplace=True, regex=True)
    df['txt'].replace('\r',' ',inplace=True, regex=True)
    df['txt'].replace(' +', ' ',inplace=True)
    df=df.fillna(0)
    
    df=df.sort_values(by, ascending=False).reset_index(drop=True).copy()
    return df

def calc_score(df):
    df['score']=(df['标化观看率']+df['标化点赞率']*2+df['标化转帖率']*5+df['标化留言率']*15)
    return df

# 重新计算时间
def set_df_timezone(df,timezone='Asia/Shanghai',summer=False):
    if summer==True:
        df['发帖时间']=df['发帖时间']+pd.Timedelta(hours=-1)
    df['发帖时间']=df['发帖时间'].dt.tz_convert(timezone)
    df['date']=df['发帖时间'].dt.date
    df['hour']=df['发帖时间'].dt.hour
    df['minute']=df['发帖时间'].dt.minute
    return df
    
    
# 将高排名的帖子存为csv
def rankposts_tocsv(df,topcount=30,lans='(汉语)'):
    name=lans+'互动度最高的'+str(topcount)+'个帖子'
    #print(savefilename,name)
    dftoppost=df.head(topcount).copy()
    dftoppost.to_csv('ranks/'+savefilename+name+'.csv',index=False,header=True)

    name=lans+'活跃度和互动最高的'+str(topcount)+'个账号'
    #print(savefilename,name)
    dftopuser=df.groupby(['uid']).sum().sort_values(by=['score'], ascending=False).drop(['hour','minute'],axis=1).head(topcount).copy()
    dftopuser.to_csv('ranks/'+savefilename+name+'.csv',index=True,header=True)

    name=lans+'互动度最高的'+str(topcount)+'个账号'
    #print(savefilename,name)
    dftoppop=dftoint(df.groupby(['uid']).mean().sort_values(by=['score'], ascending=False).drop(['hour','minute'],axis=1).head(topcount)).copy()
    dftoppop.to_csv('ranks/'+savefilename+name+'.csv',index=True,header=True)
    return dftoppost,dftopuser,dftoppop
    
# 设置图片格式
def set_ax(ax,title,**kargs):
    ax.set_ylim(ymin = 0)
    ax.set_xlim(xmin = -0.5,xmax=23.5)
    ax.set_xticks(range(0, 24), labels=range(0, 24))
    ax.set(xlabel='', ylabel='Value',title=title)
    ax.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
    return ax

# 画线图
def lineplotresult(df,ax=None,cols=[''],title='GETTR数据分析',**kargs):
    dfsns=df[cols].melt('hour')
    
    ax=sns.lineplot(data=dfsns,x='hour',y='value',hue='variable',ax=ax,legend='brief',**kargs)
    ax=set_ax(ax,title=title)
    return ax

# 画条形图
def barplotresult(df,ax=None,cols=[''],title='GETTR数据分析',**kargs):
    dfsns=df[cols].melt('hour')
    
    ax=sns.barplot(data=dfsns,x='hour',y='value',hue='variable',ax=ax,errwidth=1.5,capsize=0.2,**kargs)
    ax=set_ax(ax,title=title)
    return ax

# 画计数图
def counplotresult(df,ax=None,cols=[''],title='GETTR数据分析',**kargs):
    dfsns=df[cols].melt('hour')
    
    ax=sns.countplot(data=dfsns,x='hour',hue='variable',ax=ax,**kargs)
    ax=set_ax(ax,title=title)
    return ax

# 画全部的图
def plotall(df,savefilename,titlesufix='',timezone='(UTC+8)',show=False,topcount=30):
    cols_to_calcmean=['hour','阅读次数','点赞数','转贴数','留言数','观看率','点赞率','转帖率','留言率','标化观看率','标化点赞率','标化转帖率','标化留言率','score','标化score']
    df=df[cols_to_calcmean]
    
    cols1=['阅读次数','hour']
    cols2=['点赞数','转贴数','留言数','hour']
    cols3=['点赞率','转帖率','留言率','hour']
    cols4=['标化观看率','标化点赞率','标化转帖率','标化留言率','标化score','hour']
    cols5=['score','hour']
    fig, ax = plt.subplots(nrows=3, ncols=2, figsize=(20, 14), constrained_layout=True)
    lineplotresult(df,ax=ax[0][0],cols=cols1,title='1. 阅读次数与时间段的关系'+titlesufix)
    lineplotresult(df,ax=ax[0][1],cols=cols2,title='2. 点赞数、转贴数、留言数与时间段的关系'+titlesufix,ci=None)
    lineplotresult(df,ax=ax[1][0],cols=cols3,title='3. 点赞率、转帖率、留言率与时间段的关系'+titlesufix,ci=None)
    lineplotresult(df,ax=ax[1][1],cols=cols4,title='4. 标化后各值与时间段的关系'+titlesufix,ci=None)
    lineplotresult(df,ax=ax[2][0],cols=cols5,title='5. score与时间段的关系'+titlesufix)
    counplotresult(df,ax=ax[2][1],cols=cols5,title='6. 发帖数量与时间段的关系'+titlesufix)
    ax[2][0].set(xlabel='Hour of Day '+timezone)
    ax[2][1].set(xlabel='Hour of Day '+timezone)
    plt.savefig('plot/'+savefilename+titlesufix+'line.png',dpi=200, bbox_inches='tight')
    if show==False:
        plt.close()
        

# 打印报告
def print_summary(savefilename,df_zh,df_jp,df_en,df_all):
    pct_zh=df_zh.shape[0]/df_all.shape[0]
    pct_jp=df_jp.shape[0]/df_all.shape[0]
    pct_en=df_en.shape[0]/df_all.shape[0]
    lines=['日期'+savefilename,
           '汉语发帖数 {} ({:.2%})'.format(df_zh.shape[0],pct_zh),
           '日语发帖数 {} ({:.2%})'.format(df_jp.shape[0],pct_jp),
           ' 英语发帖数 {} ({:.2%})'.format(df_en.shape[0],pct_en),
           ' 总发帖数 {}'.format(df_all.shape[0])]
    return ' '.join(lines)

# 生成文本
def post_report_txt(df,summarytext,savefilename,lans='汉语',topcount=30,printaccount=True):
    pd.set_option('display.max_colwidth', 100)
    pd.set_option('display.float_format',lambda x : '%.3f' % x)

    summarytext=';'+summarytext
    print(summarytext,'\n')
    
    name=lans+'-互动度最高的'+str(topcount)+'个帖子(根据score排名)'
    dfls=df.head(topcount).copy()
    dfls['pid']='[https://gettr.com/post/'+dfls.copy()['pid']+' '+dfls.copy()['pid']+']'
    dfls['uid']='[https://gettr.com/user/'+dfls.copy()['uid']+' '+dfls.copy()['uid']+']'

    dfls=remove_df_index(dfls,'#')
    dfls=dfls.loc[:,['score','pid','uid','txt']]
    dfls.columns = ['', '', '','']
    print(';'+savefilename,name,end='')
    print(dfls,'\n\n')

    #########################
    if printaccount==False:
        return
    name=lans+'-活跃度和互动最高的'+str(topcount)+'个账号(score日总和)'

    dfls=df.groupby(['uid']).sum().sort_values(by=['score'], ascending=False).drop(['hour','minute'],axis=1).head(topcount).copy()
    dfls['uid']=dfls.index

    dfls['uid']='[https://gettr.com/user/'+dfls.copy()['uid']+' '+dfls.copy()['uid']+']'

    dfls=remove_df_index(dfls,'#')
    dfls=dfls.loc[:,['score','uid']]
    dfls.columns = ['', '']
    print(';'+savefilename,name,end='')
    print(dfls,'\n\n')


    #########################
    name=lans+'-互动度最高的'+str(topcount)+'个账号(score日平均值)'

    dfls=dftoint(df.groupby(['uid']).mean().sort_values(by=['score'], ascending=False).drop(['hour','minute'],axis=1).head(topcount)).copy()
    dfls['uid']=dfls.index

    dfls['uid']='[https://gettr.com/user/'+dfls.copy()['uid']+' '+dfls.copy()['uid']+']'

    dfls=remove_df_index(dfls,'#')
    dfls=dfls.loc[:,['score','uid']]
    dfls.columns = ['', '']
    print(';'+savefilename,name,end='')
    print(dfls)
    
# 网页中嵌入CSS
def combine_html(html_text):
    css_text='''
    <style  type="text/css" >
    table {
      border: none;
      border-collapse: collapse;
      border-spacing: 0;
      color: black;
      font-size: 12px;
      table-layout: fixed;
    }
    thead {
      border-bottom: 1px solid black;
      vertical-align: bottom;
    }
    tr, th, td {
      text-align: right;
      vertical-align: middle;
      padding: 0.5em 0.5em;
      line-height: normal;
      white-space: normal;
      max-width: none;
      border: none;
    }
    th {
      font-weight: bold;
    }
    tbody tr:nth-child(odd) {
      background: #f5f5f5;
    }
    tbody tr:hover {
      background: rgba(66, 165, 245, 0.2);
    }
    </style>
    '''
    html_text=css_text+html_text
    return html_text
    
# 整理帖子表格
def make_html(dfbak,keywords=['-'],lang=['zh','tw'],nums=100,blackuidlist=[]):
    df=None
    df=dfbak.copy().drop(['date','hour','minute','标化观看率','标化点赞率','标化转帖率','标化留言率','标化score'],axis=1)
    df=flt_lang(df,langs=lang)
    df=flt_txt(df,keywords=keywords,keysep=False,stopwords=['-'],stopsep=True,by='score')
    df=flt_uid(df,blackuidlist=blackuidlist)
    
    df['pid']='<a href="https://gettr.com/post/'+df['pid']+'" target="_blank">'+df['pid']+'</a>'
    df['uid']='<a href="https://gettr.com/user/'+df['uid']+'" target="_blank">@'+df['uid']+'</a>'
    html_text=combine_html(df.head(nums).to_html(escape=False, render_links=True,classes='outp',float_format='%.3f'))
    
    return html_text