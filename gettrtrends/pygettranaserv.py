from pygettrana import *
import tornado.httpserver, tornado.ioloop, tornado.options, tornado.web, tornado.autoreload
import os, wget, sys, getopt, gzip, datetime
from datetime import timezone
# import nest_asyncio
# nest_asyncio.apply()


dfbak=None
cols_to_drop=['udate','prevsrc','previmg','prev_lang','htgs','utgs','max_viewers','vid','vid_wid','vid_hgt','ttl','dsc','txt_zhs','txt_zht','txt_en']


# 启动参数传入
def py_init(argv):
    print('\n\n==========pyGETTR Trends program==========')
    startdate='99999999'
    enddate='20000101'
    downhour='_0017'
    filepath=''
    try:
        opts, args = getopt.getopt(argv,"hdf:",["help","date=","file="])
    except getopt.GetoptError:
        print('python mytornado.py --date=20220114 or now or 20220101-20220114')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('python mytornado.py --date=20220114 or now or 20220101-20220114')
            sys.exit()
        elif opt in ("-d", "--date"):
            if arg=="now":
                now = datetime.datetime.now(datetime.timezone.utc)
                year = '{:04d}'.format(now.year)
                month = '{:02d}'.format(now.month)
                day = '{:02d}'.format(now.day)
                hour = '{:02d}'.format(now.hour-1)
                startdate=year+month+day
                downhour="_"+hour+"17"
            else:
                if "-" not in arg: 
                    startdate = arg
                else:
                    startdate,enddate=arg.split("-")
        elif opt in ("-f", "--file"):
            startdate='99999999'
            filepath=arg
    return startdate,enddate,downhour,filepath


# 载入数据
def read_df_withdeal(filelist,downpath='down/'):
    df=get_df_fromlist(filelist,downpath=downpath)
    
    df=set_df_timezone(df,timezone='America/New_York',summer=True)
    
    print(nowtime(),"Calculating scores ...")
    df=dealdf(df,by='score',min_follower=777)
    
    return df

def read_df_fromfile(filepath):
    df=get_df_fromfile(filepath,downpath='')
    return df

# 生成临时文件
def use_cache(csv_name,df=None):
    path='cache/'
    filepath=path+csv_name
    makedir('cache') 
        
    if not os.path.exists(filepath):
        df.to_csv(filepath)
        print(nowtime(),'csv created.',filepath)
    else:
        
        print(nowtime(),'csv Found:',filepath)

# 首页
class MainHandler(tornado.web.RequestHandler):
    def get(self):
        # make_html()
        self.render('pyGettrTrendsindex.html')

# 处理API
class ReturnQuery(tornado.web.RequestHandler):
    def get(self):
        keywords = self.get_argument('txt')
        langs = self.get_argument('langs')
        nums = self.get_argument('nums')
        blackuidlist = self.get_argument('blackuidlist')
        if nums=="":
            nums=100
        else:
            nums=int(nums)
        if langs=='zh':
            langs='zh tw'
        print(nowtime(),keywords.split(),langs.split(),nums,"BlackList =",blackuidlist,self.request.remote_ip)
        html_text=make_html(dfbak,keywords.split(),langs.split(),nums,blackuidlist=blackuidlist)
        self.write(html_text)
        # self.render('output.html')
        
# 停止程序
class StopTornado(tornado.web.RequestHandler):
    def get(self):
        tornado.ioloop.IOLoop.instance().stop()

# 主程序
class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", MainHandler),
            (r"/k7777777", StopTornado),
            (r"/t", ReturnQuery)
        ]
        settings = {
            "static_path": os.path.join(os.path.dirname(__file__), "static")
        }
        tornado.web.Application.__init__(self, handlers, **settings)

# 主程序
if __name__ == "__main__":
    
    startdate,enddate,downhour,filepath=py_init(sys.argv[1:])
    
    if filepath=="":
        datelist=create_date_list(startdate,enddate)

        filelist=create_csv_filelist(datelist,downhour)
        downpath='down/'
        
        dfbak=read_df_withdeal(filelist=filelist,downpath=downpath)
    else:
        dfbak=read_df_fromfile(filepath)
        dfbak=df_init(dfbak,cols_to_drop)
        dfbak=set_df_timezone(dfbak,timezone='America/New_York',summer=True)
    
        print(nowtime(),"Calculating scores ...")
        dfbak=dealdf(dfbak,by='score',min_follower=777)
        
    
    
    
    app = Application()
    
    app.listen(8000)
    print(nowtime(),'Listening port 8000')
    tornado.autoreload.start()
    tornado.autoreload.watch('index.html')
    tornado.ioloop.IOLoop.current().start()