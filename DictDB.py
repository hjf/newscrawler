import Cache
import DbEngine
import collections
import DTime
import Log
import queue
import threading
import time
import bisect
import random

class RawDB():
    def __init__(self, dbName, tableName, keyField, drop):
        self.log = Log.Log(Log.DEBUG_LEVEL, who='rawdb')
        self.dbName = dbName
        self.tableName = tableName
        self.keyField = keyField
        self.engine = DbEngine.DbEngine(dbName)
        self.lock = threading.Lock()
        if drop:
            self.engine.dropTable(tableName)

    def __del__(self):
        self.log.D("RawDB __del__", self.dbName, self.tableName)

    def stop(self):
        self.engine = None

    def execSQL(self, sql):
        with self.lock:
            self.engine.execSQL(sql)

    def set(self, kv, item, callback=None):
        with self.lock:
            assert self.keyField in item
            assert kv == item[self.keyField]
            self.engine.set(self.tableName, self.keyField, item, callback)

    def get(self, kv):
        with self.lock:
            return self.engine.get(self.tableName, self.keyField, kv)

    def count(self):
        with self.lock:
            return self.engine.count(self.tableName)

    def query(self, sql, *args):
        with self.lock:
            return self.engine.query(self.tableName, self.keyField, sql, *args)

    def existTable(self):
        with self.lock:
            return self.engine.existTable(self.tableName)

class DictDB():
    def __init__(self,
                 dbName         = "test",
                 tableName      = "test",
                 keyField       = "id",
                 drop           = False,
                 minCache       = 7,
                 maxCache       = 0,
                 ioCache        = 7777,
                 maxMemPercent  = 77.7):
        self.log = Log.Log(Log.VERBOSE_LEVEL, who='dicdb')
        self.log.D("create DB:", dbName, "Table:", tableName, "keyField:", keyField, "drop:", drop,
              "minCache:", minCache,
              "maxCache:", maxCache,
              "ioCache:", ioCache,
              "maxMemPercent:", maxMemPercent)

        self.dbName = dbName
        self.tableName = tableName
        self.keyField = keyField
        self.rawDB = RawDB(dbName, tableName, keyField, drop)
        if not self.rawDB.existTable():
            self.createTable()

        self.cache = Cache.Cache(name=tableName,
                                minCache=minCache,
                                maxCache=maxCache,
                                ioCache=ioCache,
                                maxMemPercent=maxMemPercent,
                                getFunc = self.rawDB.get,
                                setFunc = self.rawDB.set,
                                queryFunc = self.rawDB.query)
    def __del__(self):
        self.log.D("__del__", self.dbName, self.tableName)
        self.cache.stop()
        self.rawDB.stop()

    def createTable(self):
        self.rawDB.execSQL("""CREATE TABLE IF NOT EXISTS test (
            id int primary key,
            v int
            );""")

    def count(self):
        return max(self.rawDB.count(), self.cache.size())

    def set(self, item, allowReplace = True):
        assert self.keyField in item
        kv = item[self.keyField]
        return self.cache.set(kv, item, allowReplace = allowReplace)

    def get(self, kv, needClone=True):
        return self.cache.get(kv, needClone)

    def flush(self):
        return self.cache.flush(block=True)

    def query(self, sql, *args):
        self.cache.flush(block=True)
        return self.cache.query(sql, *args)

def test_basic0():
    log = Log.Log()

    db = DictDB('test', 'test', 'id', True)

    D = {}
    N = 1000
    log.D('set data')
    for i in range(N):
        data = {'id':i, 'v':i}
        db.set(data)
        D[i] = i

    log.D('get data')
    for i in range(N):
        data = db.get(i)
        assert data['v'] == i

    log.D('random set')
    for i in range(N):
        x = random.randrange(N)
        y = random.randrange(N)
        data = {'id':x, 'v':y}
        db.set(data, data)
        D[x] = y

    log.D('random get')
    for i in range(N):
        x = random.randrange(N)
        data = db.get(x)
        assert D[x] == data['v']


def test_basic1():
    class MyDB(DictDB):
        def createTable(self):
            self.rawDB.execSQL("""CREATE TABLE IF NOT EXISTS test (
                id int primary key,
                v int,
                w int,
                x int,
                y int,
                z int
                );""")
    db = MyDB('test', 'test', 'id', True)
    assert db.count() == 0

    N=1000
    for i in range(N):
        db.set({'id':i, 'x':i*i})
    for i in range(N):
        db.set({'id':i, 'v':i*2})

    for i in range(N):
        item = db.get(i)
        assert item
        assert item['id'] == i
        assert item['x'] == i*i
        assert item['v'] == i*2

    for i in range(N):
        db.set({'id':i, 'x':i*i*i})
        item = db.get(i)
        assert item['x'] == i*i*i

    for i in range(N):
        db.set({'id':i, 'y':i*i})

    for i in range(N):
        db.set({'id':i, 'x':i})
    for i in range(N):
        db.set({'id':i, 'x':i+1}, allowReplace=False)
    for i in range(N):
        db.set({'id':i, 'w':i*10})
    for i in range(N):
        db.set({'id':i, 'z':i})


def test_basic2():
    class MyDB(DictDB):
        def createTable(self):
            self.rawDB.execSQL("""CREATE TABLE IF NOT EXISTS test (
                id int primary key,
                v int,
                w int,
                x int,
                y int,
                z int
                );""")
    log = Log.Log()
    db = MyDB('test', 'test', 'id', True)
    N=10
    for i in range(N):
        db.set({'id':i, 'x':i*i})

    db.flush()

    db = MyDB('test', 'test', 'id', False)
    X = db.query("select * from test where (x%2)=0 order by x desc offset 2 limit 2")
    assert len(X) == 2
    assert X[0] == 4
    assert X[1] == 2


if __name__ == "__main__":
    test_basic0()
    test_basic1()
    test_basic2()
