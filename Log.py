import collections
import DTime
import sys
import os
import threading
import time
import traceback
import random
import queue
import io

ERROR_LEVEL=0
WARNING_LEVEL=1
DEBUG_LEVEL=2
VERBOSE_LEVEL=3

COLOR_DARK_BLUE = "\033[1;36m"
COLOR_R = "\033[1;31m"
COLOR_Y = "\033[1;33m"
COLOR_G = "\032[1;33m"
COLOR_N = "\033[m"

COLOR256_31 = "\033[38;5;31m"
COLOR256_86 = "\033[38;5;86m"
COLOR256_111 = "\033[38;5;111m"
COLOR256_112 = "\033[38;5;112m"
COLOR256_196 = "\033[38;5;196m"
COLOR256_200 = "\033[38;5;200m"
COLOR256_208 = "\033[38;5;208m"
COLOR256_211 = "\033[38;5;211m"


gLogLock = threading.Lock()
gLogWorker = None
gLogCount = 0

class LogWorker():
    def __init__(self):
        #print("LogWorker born")
        self.queue = queue.Queue()
        self.dead = threading.Event()
        self.thread = threading.Thread(target = self.process, name="LogWorker")
        self.thread.start()

    def stop(self):
        #print("LogWorker stop()")
        if not self.dead.is_set():
            self.queue.put(None)
            self.dead.wait()

    def __del__(self):
        #print("LogWorker die")
        pass

    def enque(self, X):
        if X:
            self.queue.put(X)

    def dump(self):

        T = {}
        for t in threading.enumerate():
            T[t.ident] = t

        for thread_id, frame in sys._current_frames().items():
            t = T[thread_id]
            print(f">>>>>>> Stack for thread({t.name}) {t.ident} {t.native_id} <<<<<<<")
            traceback.print_stack(frame)
            print("")

    def process(self):
        MT = threading.main_thread()
        lastT = None
        wcnt = 0
        while True:
            try:
                X = self.queue.get(timeout=14)
                wcnt = 0
                if X:
                    t = X[0]

                    if lastT == None:
                        d = 0
                    else:
                        d = (t - lastT).total_seconds()

                    #if lastT == None or d>=7:
                    #    print(COLOR_DARK_BLUE+f'[{t}] {d:2.3f}s'+COLOR_N)
                    #    lastT = t

                    print(t.strftime("%m-%d %H:%M:%S")+f'+{d:2.2f}', X[1])
                    lastT = t

                else:
                    break
            except queue.Empty:
                if not MT.is_alive():
                    global gLogCount
                    print("\n\n")
                    print(COLOR256_200+"======= LogWorker detect main thread dead. Force Exit!!! Log:", gLogCount ," ======="+COLOR_N)
                    self.dump()
                    os._exit(1)
                    break
                elif lastT != None and (DTime.now() - lastT).total_seconds() >= 17:
                    print("\n\n")
                    wcnt +=1
                    print(COLOR256_200+f"======= LogWorker watchdog timeout ({wcnt}/3)======="+COLOR_N)
                    self.dump()
                    lastT = DTime.now()
                    if wcnt>=3:
                        os._exit(1)

        self.dead.set()
        #print("LogWorker exit")

class Log():
    def __init__(self, level = DEBUG_LEVEL, who = ''):
        global gLogLock
        global gLogCount
        global gLogWorker
        with gLogLock:
            if gLogCount == 0:
                gLogWorker = LogWorker()
            gLogCount += 1

        self.who = who
        self.printLock = threading.Lock()
        self.lifeStart = DTime.now()
        self.lastTime = DTime.now()
        self.logLevel = ERROR_LEVEL
        self.lastTimeStr = collections.OrderedDict()
        self.timeIt = collections.OrderedDict()
        self.lastTimeStrMax = 77777
        self.timeItMax = 77777
        self.logLevel = level
        self.textColor = ''

        #self.D("LogCount+", self.who, gLogCount)

    def __del__(self):
        global gLogLock
        global gLogCount
        global gLogWorker
        with gLogLock:
            gLogCount -= 1
            #self.D("LogCount-", self.who, gLogCount)
            if gLogCount == 0:
                gLogWorker.stop()
                gLogWorker = None

    def getLogLevel(self):
        return self.logLevel

    def setLogLevel(self, level):
        self.logLevel = level
        return self.logLevel

    def setTextColor(self, textColor):
        self.textColor = textColor

    def LOG(self, logStr, *msg, minGap, timeIt, bypass, lifeTime, sum):
        with self.printLock:
            tid = threading.get_native_id()
            t = DTime.now()

            if lifeTime:
                lc = str(t - self.lifeStart)
                lifeTimeStr = f"[life cycle {lc:s}]"
            else:
                lifeTimeStr = ""

            if timeIt:
                if type(timeIt) == bool:
                    k = msg[0]
                else:
                    k = timeIt
                if k not in self.timeIt:
                    self.timeIt[k] = [t, 0.0, 0, 0]
                    if len(self.timeIt) > self.timeItMax:
                        self.timeIt.popitem(last = False)

                if bypass:
                    self.timeIt[k][0] = t
                else:
                    self.timeIt[k][2] += 1
                    self.timeIt[k][3] += sum

                tmp = (t - self.timeIt[k][0]).total_seconds()

                self.timeIt[k][1] += tmp
                tt = self.timeIt[k][1]
                cnt = self.timeIt[k][2]
                if cnt > 0:
                    avg = tt / cnt
                else:
                    avg = 0

                if self.timeIt[k][3]>0:
                    SUMSTR = f" S:{self.timeIt[k][3]:d}"
                else:
                    SUMSTR = ''

                timeItStr = f" [T:{tmp:.3f}s A:{avg:.3f} C:{cnt:d}{SUMSTR:s}]"
                self.timeIt[k][0] = t
            else:
                timeItStr = ""

            if minGap >0:
                k = msg[0]
                if k not in self.lastTimeStr:
                    self.lastTimeStr[k] = t
                    if len(self.lastTimeStr) > self.lastTimeStrMax:
                        self.lastTimeStr.popitem(last = False)
                elif (t - self.lastTimeStr[k]).total_seconds() < minGap:
                    return
                self.lastTimeStr[k] = t
                minGapStr = '..'
            else:
                minGapStr = ''

            d = (t - self.lastTime).total_seconds()
            self.lastTime = t

            if not bypass:
                try:
                    out = io.StringIO()
                    tname = threading.current_thread().name
                    if tname=='MainThread':
                        tname='main'
                    if tname==self.who:
                        whoStr = f'[{tname:11s}]'
                    else:
                        whoStr = f'[{tname:5s}>{self.who:5s}]'

                    print(whoStr, logStr+self.textColor+timeItStr, *msg, minGapStr+COLOR_N, lifeTimeStr, file=out, end='')
                    gLogWorker.enque((t, out.getvalue()))
                except:
                    traceback.print_exc()

    def F(self, cond, *msg, timeIt = False):
        if not cond:
            self.LOG("[F]", *msg, minGap = 0, timeIt = timeIt, bypass = False, lifeTime = False, sum = 0)
            assert cond

    def E(self, *msg, minGap = 0, timeIt = False, bypass = False, lifeTime = False, sum=0):
        if self.logLevel >= ERROR_LEVEL:
            self.LOG(COLOR_R+"[E]", *msg, COLOR_N, minGap = minGap, timeIt = timeIt, bypass = bypass, lifeTime = lifeTime, sum = sum)

    def W(self, *msg, minGap = 0, timeIt = False, bypass = False, lifeTime = False, sum=0):
        if self.logLevel >= WARNING_LEVEL:
            self.LOG(COLOR_Y+"[W]", *msg, COLOR_N, minGap = minGap, timeIt = timeIt, bypass = bypass, lifeTime = lifeTime, sum = sum)

    def D(self, *msg, minGap = 0, timeIt = False, bypass = False, lifeTime = False, sum=0):
        if self.logLevel >= DEBUG_LEVEL:
            self.LOG("[D]", *msg, minGap = minGap, timeIt = timeIt, bypass = bypass, lifeTime = lifeTime, sum = sum)

    def V(self, *msg, minGap = 0, timeIt = False, bypass = False, lifeTime = False, sum=0):
        if self.logLevel >= VERBOSE_LEVEL:
            self.LOG("[V]", *msg, minGap = minGap, timeIt = timeIt, bypass = bypass, lifeTime = lifeTime, sum = sum)

def test_setloglevel():
    L = Log()
    assert L.setLogLevel(ERROR_LEVEL) == ERROR_LEVEL
    assert L.setLogLevel(DEBUG_LEVEL) ==  DEBUG_LEVEL
    assert L.setLogLevel(VERBOSE_LEVEL) == VERBOSE_LEVEL

def test_print():
    L = Log(VERBOSE_LEVEL)
    L.E("a",1,"b",2.3)
    L.D("a",1,"b",2.3, timeIt = True, bypass = True)
    L.V("a",1,"b",2.3)
    L.D("xx",1, minGap = 10)
    time.sleep(0.5)
    L.D("a",1,"b",2.3, timeIt = True, lifeTime = True)
    try:
        F(False, "aa", 1,2,3)
        assert False
    except:
        assert True

def test_threading():
    L = Log()
    def worker(id):
        for x in range(77):
            L.D("thread", id, "num", x, "X" * random.randrange(100), timeIt=True, lifeTime=True)

    T = {}
    for i in range(17):
        T[i] = threading.Thread(target=worker, args=(i, ))
        T[i].start()

    for i in range(17):
        T[i].join()


if __name__ == "__main__":
    test_setloglevel()
    test_print()
    test_threading()

